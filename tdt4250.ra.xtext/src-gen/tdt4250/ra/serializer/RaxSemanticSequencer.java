/*
 * generated by Xtext 2.22.0
 */
package tdt4250.ra.serializer;

import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import tdt4250.ra.Course;
import tdt4250.ra.Department;
import tdt4250.ra.Person;
import tdt4250.ra.RaPackage;
import tdt4250.ra.ResourceAllocation;
import tdt4250.ra.Role;
import tdt4250.ra.services.RaxGrammarAccess;

@SuppressWarnings("all")
public class RaxSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private RaxGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == RaPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case RaPackage.COURSE:
				sequence_Course(context, (Course) semanticObject); 
				return; 
			case RaPackage.DEPARTMENT:
				sequence_Department(context, (Department) semanticObject); 
				return; 
			case RaPackage.PERSON:
				sequence_Person(context, (Person) semanticObject); 
				return; 
			case RaPackage.RESOURCE_ALLOCATION:
				sequence_ResourceAllocation(context, (ResourceAllocation) semanticObject); 
				return; 
			case RaPackage.ROLE:
				sequence_Role(context, (Role) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Course returns Course
	 *
	 * Constraint:
	 *     (code=ID name=STRING (roles+=Role roles+=Role* resourceAllocations+=ResourceAllocation+)?)
	 */
	protected void sequence_Course(ISerializationContext context, Course semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Department returns Department
	 *
	 * Constraint:
	 *     (shortName=ID name=STRING staff+=Person* courses+=Course*)
	 */
	protected void sequence_Department(ISerializationContext context, Department semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Person returns Person
	 *
	 * Constraint:
	 *     name=ID_OR_STRING
	 */
	protected void sequence_Person(ISerializationContext context, Person semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.PERSON__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.PERSON__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getPersonAccess().getNameID_OR_STRINGParserRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     ResourceAllocation returns ResourceAllocation
	 *
	 * Constraint:
	 *     (person=[Person|ID_OR_STRING] factor=Factor role=[Role|ID])
	 */
	protected void sequence_ResourceAllocation(ISerializationContext context, ResourceAllocation semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__PERSON) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__PERSON));
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__FACTOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__FACTOR));
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__ROLE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.RESOURCE_ALLOCATION__ROLE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getResourceAllocationAccess().getPersonPersonID_OR_STRINGParserRuleCall_0_0_1(), semanticObject.eGet(RaPackage.Literals.RESOURCE_ALLOCATION__PERSON, false));
		feeder.accept(grammarAccess.getResourceAllocationAccess().getFactorFactorParserRuleCall_2_0(), semanticObject.getFactor());
		feeder.accept(grammarAccess.getResourceAllocationAccess().getRoleRoleIDTerminalRuleCall_4_0_1(), semanticObject.eGet(RaPackage.Literals.RESOURCE_ALLOCATION__ROLE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Role returns Role
	 *
	 * Constraint:
	 *     (name=ID factor=Factor)
	 */
	protected void sequence_Role(ISerializationContext context, Role semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.ROLE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.ROLE__NAME));
			if (transientValues.isValueTransient(semanticObject, RaPackage.Literals.ROLE__FACTOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, RaPackage.Literals.ROLE__FACTOR));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getRoleAccess().getFactorFactorParserRuleCall_2_0(), semanticObject.getFactor());
		feeder.finish();
	}
	
	
}
