package tdt4250.ra.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import tdt4250.ra.services.RaxGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRaxParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'-'", "'Persons:'", "'Courses:'", "'requires'", "','", "'provided-by'", "'is'", "'as'", "'.'", "'for'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalRaxParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRaxParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRaxParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRax.g"; }



     	private RaxGrammarAccess grammarAccess;

        public InternalRaxParser(TokenStream input, RaxGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Department";
       	}

       	@Override
       	protected RaxGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDepartment"
    // InternalRax.g:64:1: entryRuleDepartment returns [EObject current=null] : iv_ruleDepartment= ruleDepartment EOF ;
    public final EObject entryRuleDepartment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDepartment = null;


        try {
            // InternalRax.g:64:51: (iv_ruleDepartment= ruleDepartment EOF )
            // InternalRax.g:65:2: iv_ruleDepartment= ruleDepartment EOF
            {
             newCompositeNode(grammarAccess.getDepartmentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDepartment=ruleDepartment();

            state._fsp--;

             current =iv_ruleDepartment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDepartment"


    // $ANTLR start "ruleDepartment"
    // InternalRax.g:71:1: ruleDepartment returns [EObject current=null] : ( ( (lv_shortName_0_0= RULE_ID ) ) otherlv_1= '-' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= 'Persons:' ( (lv_staff_4_0= rulePerson ) )* otherlv_5= 'Courses:' ( (lv_courses_6_0= ruleCourse ) )* ) ;
    public final EObject ruleDepartment() throws RecognitionException {
        EObject current = null;

        Token lv_shortName_0_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_staff_4_0 = null;

        EObject lv_courses_6_0 = null;



        	enterRule();

        try {
            // InternalRax.g:77:2: ( ( ( (lv_shortName_0_0= RULE_ID ) ) otherlv_1= '-' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= 'Persons:' ( (lv_staff_4_0= rulePerson ) )* otherlv_5= 'Courses:' ( (lv_courses_6_0= ruleCourse ) )* ) )
            // InternalRax.g:78:2: ( ( (lv_shortName_0_0= RULE_ID ) ) otherlv_1= '-' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= 'Persons:' ( (lv_staff_4_0= rulePerson ) )* otherlv_5= 'Courses:' ( (lv_courses_6_0= ruleCourse ) )* )
            {
            // InternalRax.g:78:2: ( ( (lv_shortName_0_0= RULE_ID ) ) otherlv_1= '-' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= 'Persons:' ( (lv_staff_4_0= rulePerson ) )* otherlv_5= 'Courses:' ( (lv_courses_6_0= ruleCourse ) )* )
            // InternalRax.g:79:3: ( (lv_shortName_0_0= RULE_ID ) ) otherlv_1= '-' ( (lv_name_2_0= RULE_STRING ) ) otherlv_3= 'Persons:' ( (lv_staff_4_0= rulePerson ) )* otherlv_5= 'Courses:' ( (lv_courses_6_0= ruleCourse ) )*
            {
            // InternalRax.g:79:3: ( (lv_shortName_0_0= RULE_ID ) )
            // InternalRax.g:80:4: (lv_shortName_0_0= RULE_ID )
            {
            // InternalRax.g:80:4: (lv_shortName_0_0= RULE_ID )
            // InternalRax.g:81:5: lv_shortName_0_0= RULE_ID
            {
            lv_shortName_0_0=(Token)match(input,RULE_ID,FOLLOW_3); 

            					newLeafNode(lv_shortName_0_0, grammarAccess.getDepartmentAccess().getShortNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDepartmentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"shortName",
            						lv_shortName_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,11,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDepartmentAccess().getHyphenMinusKeyword_1());
            		
            // InternalRax.g:101:3: ( (lv_name_2_0= RULE_STRING ) )
            // InternalRax.g:102:4: (lv_name_2_0= RULE_STRING )
            {
            // InternalRax.g:102:4: (lv_name_2_0= RULE_STRING )
            // InternalRax.g:103:5: lv_name_2_0= RULE_STRING
            {
            lv_name_2_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

            					newLeafNode(lv_name_2_0, grammarAccess.getDepartmentAccess().getNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDepartmentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getDepartmentAccess().getPersonsKeyword_3());
            		
            // InternalRax.g:123:3: ( (lv_staff_4_0= rulePerson ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_ID && LA1_0<=RULE_STRING)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalRax.g:124:4: (lv_staff_4_0= rulePerson )
            	    {
            	    // InternalRax.g:124:4: (lv_staff_4_0= rulePerson )
            	    // InternalRax.g:125:5: lv_staff_4_0= rulePerson
            	    {

            	    					newCompositeNode(grammarAccess.getDepartmentAccess().getStaffPersonParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_6);
            	    lv_staff_4_0=rulePerson();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDepartmentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"staff",
            	    						lv_staff_4_0,
            	    						"tdt4250.ra.Rax.Person");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getDepartmentAccess().getCoursesKeyword_5());
            		
            // InternalRax.g:146:3: ( (lv_courses_6_0= ruleCourse ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRax.g:147:4: (lv_courses_6_0= ruleCourse )
            	    {
            	    // InternalRax.g:147:4: (lv_courses_6_0= ruleCourse )
            	    // InternalRax.g:148:5: lv_courses_6_0= ruleCourse
            	    {

            	    					newCompositeNode(grammarAccess.getDepartmentAccess().getCoursesCourseParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_courses_6_0=ruleCourse();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDepartmentRule());
            	    					}
            	    					add(
            	    						current,
            	    						"courses",
            	    						lv_courses_6_0,
            	    						"tdt4250.ra.Rax.Course");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDepartment"


    // $ANTLR start "entryRuleCourse"
    // InternalRax.g:169:1: entryRuleCourse returns [EObject current=null] : iv_ruleCourse= ruleCourse EOF ;
    public final EObject entryRuleCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourse = null;


        try {
            // InternalRax.g:169:47: (iv_ruleCourse= ruleCourse EOF )
            // InternalRax.g:170:2: iv_ruleCourse= ruleCourse EOF
            {
             newCompositeNode(grammarAccess.getCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourse=ruleCourse();

            state._fsp--;

             current =iv_ruleCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourse"


    // $ANTLR start "ruleCourse"
    // InternalRax.g:176:1: ruleCourse returns [EObject current=null] : ( ( (lv_code_0_0= RULE_ID ) ) (otherlv_1= '-' )? ( (lv_name_2_0= RULE_STRING ) ) (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )? ) ;
    public final EObject ruleCourse() throws RecognitionException {
        EObject current = null;

        Token lv_code_0_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_roles_4_0 = null;

        EObject lv_roles_6_0 = null;

        EObject lv_resourceAllocations_8_0 = null;



        	enterRule();

        try {
            // InternalRax.g:182:2: ( ( ( (lv_code_0_0= RULE_ID ) ) (otherlv_1= '-' )? ( (lv_name_2_0= RULE_STRING ) ) (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )? ) )
            // InternalRax.g:183:2: ( ( (lv_code_0_0= RULE_ID ) ) (otherlv_1= '-' )? ( (lv_name_2_0= RULE_STRING ) ) (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )? )
            {
            // InternalRax.g:183:2: ( ( (lv_code_0_0= RULE_ID ) ) (otherlv_1= '-' )? ( (lv_name_2_0= RULE_STRING ) ) (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )? )
            // InternalRax.g:184:3: ( (lv_code_0_0= RULE_ID ) ) (otherlv_1= '-' )? ( (lv_name_2_0= RULE_STRING ) ) (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )?
            {
            // InternalRax.g:184:3: ( (lv_code_0_0= RULE_ID ) )
            // InternalRax.g:185:4: (lv_code_0_0= RULE_ID )
            {
            // InternalRax.g:185:4: (lv_code_0_0= RULE_ID )
            // InternalRax.g:186:5: lv_code_0_0= RULE_ID
            {
            lv_code_0_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_code_0_0, grammarAccess.getCourseAccess().getCodeIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCourseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"code",
            						lv_code_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalRax.g:202:3: (otherlv_1= '-' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==11) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalRax.g:203:4: otherlv_1= '-'
                    {
                    otherlv_1=(Token)match(input,11,FOLLOW_4); 

                    				newLeafNode(otherlv_1, grammarAccess.getCourseAccess().getHyphenMinusKeyword_1());
                    			

                    }
                    break;

            }

            // InternalRax.g:208:3: ( (lv_name_2_0= RULE_STRING ) )
            // InternalRax.g:209:4: (lv_name_2_0= RULE_STRING )
            {
            // InternalRax.g:209:4: (lv_name_2_0= RULE_STRING )
            // InternalRax.g:210:5: lv_name_2_0= RULE_STRING
            {
            lv_name_2_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_name_2_0, grammarAccess.getCourseAccess().getNameSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCourseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalRax.g:226:3: (otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+ )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==14) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalRax.g:227:4: otherlv_3= 'requires' ( (lv_roles_4_0= ruleRole ) ) (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )* otherlv_7= 'provided-by' ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_10); 

                    				newLeafNode(otherlv_3, grammarAccess.getCourseAccess().getRequiresKeyword_3_0());
                    			
                    // InternalRax.g:231:4: ( (lv_roles_4_0= ruleRole ) )
                    // InternalRax.g:232:5: (lv_roles_4_0= ruleRole )
                    {
                    // InternalRax.g:232:5: (lv_roles_4_0= ruleRole )
                    // InternalRax.g:233:6: lv_roles_4_0= ruleRole
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_roles_4_0=ruleRole();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						add(
                    							current,
                    							"roles",
                    							lv_roles_4_0,
                    							"tdt4250.ra.Rax.Role");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalRax.g:250:4: (otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) ) )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==15) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalRax.g:251:5: otherlv_5= ',' ( (lv_roles_6_0= ruleRole ) )
                    	    {
                    	    otherlv_5=(Token)match(input,15,FOLLOW_10); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getCourseAccess().getCommaKeyword_3_2_0());
                    	    				
                    	    // InternalRax.g:255:5: ( (lv_roles_6_0= ruleRole ) )
                    	    // InternalRax.g:256:6: (lv_roles_6_0= ruleRole )
                    	    {
                    	    // InternalRax.g:256:6: (lv_roles_6_0= ruleRole )
                    	    // InternalRax.g:257:7: lv_roles_6_0= ruleRole
                    	    {

                    	    							newCompositeNode(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_2_1_0());
                    	    						
                    	    pushFollow(FOLLOW_11);
                    	    lv_roles_6_0=ruleRole();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getCourseRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"roles",
                    	    								lv_roles_6_0,
                    	    								"tdt4250.ra.Rax.Role");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,16,FOLLOW_12); 

                    				newLeafNode(otherlv_7, grammarAccess.getCourseAccess().getProvidedByKeyword_3_3());
                    			
                    // InternalRax.g:279:4: ( (lv_resourceAllocations_8_0= ruleResourceAllocation ) )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==RULE_ID) ) {
                            int LA5_2 = input.LA(2);

                            if ( (LA5_2==17) ) {
                                alt5=1;
                            }


                        }
                        else if ( (LA5_0==RULE_STRING) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalRax.g:280:5: (lv_resourceAllocations_8_0= ruleResourceAllocation )
                    	    {
                    	    // InternalRax.g:280:5: (lv_resourceAllocations_8_0= ruleResourceAllocation )
                    	    // InternalRax.g:281:6: lv_resourceAllocations_8_0= ruleResourceAllocation
                    	    {

                    	    						newCompositeNode(grammarAccess.getCourseAccess().getResourceAllocationsResourceAllocationParserRuleCall_3_4_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_resourceAllocations_8_0=ruleResourceAllocation();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"resourceAllocations",
                    	    							lv_resourceAllocations_8_0,
                    	    							"tdt4250.ra.Rax.ResourceAllocation");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourse"


    // $ANTLR start "entryRuleResourceAllocation"
    // InternalRax.g:303:1: entryRuleResourceAllocation returns [EObject current=null] : iv_ruleResourceAllocation= ruleResourceAllocation EOF ;
    public final EObject entryRuleResourceAllocation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceAllocation = null;


        try {
            // InternalRax.g:303:59: (iv_ruleResourceAllocation= ruleResourceAllocation EOF )
            // InternalRax.g:304:2: iv_ruleResourceAllocation= ruleResourceAllocation EOF
            {
             newCompositeNode(grammarAccess.getResourceAllocationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceAllocation=ruleResourceAllocation();

            state._fsp--;

             current =iv_ruleResourceAllocation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceAllocation"


    // $ANTLR start "ruleResourceAllocation"
    // InternalRax.g:310:1: ruleResourceAllocation returns [EObject current=null] : ( ( ( ruleID_OR_STRING ) ) otherlv_1= 'is' ( (lv_factor_2_0= ruleFactor ) ) otherlv_3= 'as' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleResourceAllocation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_factor_2_0 = null;



        	enterRule();

        try {
            // InternalRax.g:316:2: ( ( ( ( ruleID_OR_STRING ) ) otherlv_1= 'is' ( (lv_factor_2_0= ruleFactor ) ) otherlv_3= 'as' ( (otherlv_4= RULE_ID ) ) ) )
            // InternalRax.g:317:2: ( ( ( ruleID_OR_STRING ) ) otherlv_1= 'is' ( (lv_factor_2_0= ruleFactor ) ) otherlv_3= 'as' ( (otherlv_4= RULE_ID ) ) )
            {
            // InternalRax.g:317:2: ( ( ( ruleID_OR_STRING ) ) otherlv_1= 'is' ( (lv_factor_2_0= ruleFactor ) ) otherlv_3= 'as' ( (otherlv_4= RULE_ID ) ) )
            // InternalRax.g:318:3: ( ( ruleID_OR_STRING ) ) otherlv_1= 'is' ( (lv_factor_2_0= ruleFactor ) ) otherlv_3= 'as' ( (otherlv_4= RULE_ID ) )
            {
            // InternalRax.g:318:3: ( ( ruleID_OR_STRING ) )
            // InternalRax.g:319:4: ( ruleID_OR_STRING )
            {
            // InternalRax.g:319:4: ( ruleID_OR_STRING )
            // InternalRax.g:320:5: ruleID_OR_STRING
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getResourceAllocationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getResourceAllocationAccess().getPersonPersonCrossReference_0_0());
            				
            pushFollow(FOLLOW_14);
            ruleID_OR_STRING();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,17,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getResourceAllocationAccess().getIsKeyword_1());
            		
            // InternalRax.g:338:3: ( (lv_factor_2_0= ruleFactor ) )
            // InternalRax.g:339:4: (lv_factor_2_0= ruleFactor )
            {
            // InternalRax.g:339:4: (lv_factor_2_0= ruleFactor )
            // InternalRax.g:340:5: lv_factor_2_0= ruleFactor
            {

            					newCompositeNode(grammarAccess.getResourceAllocationAccess().getFactorFactorParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_16);
            lv_factor_2_0=ruleFactor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResourceAllocationRule());
            					}
            					set(
            						current,
            						"factor",
            						lv_factor_2_0,
            						"tdt4250.ra.Rax.Factor");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,18,FOLLOW_10); 

            			newLeafNode(otherlv_3, grammarAccess.getResourceAllocationAccess().getAsKeyword_3());
            		
            // InternalRax.g:361:3: ( (otherlv_4= RULE_ID ) )
            // InternalRax.g:362:4: (otherlv_4= RULE_ID )
            {
            // InternalRax.g:362:4: (otherlv_4= RULE_ID )
            // InternalRax.g:363:5: otherlv_4= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getResourceAllocationRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_4, grammarAccess.getResourceAllocationAccess().getRoleRoleCrossReference_4_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceAllocation"


    // $ANTLR start "entryRuleFactor"
    // InternalRax.g:378:1: entryRuleFactor returns [String current=null] : iv_ruleFactor= ruleFactor EOF ;
    public final String entryRuleFactor() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFactor = null;


        try {
            // InternalRax.g:378:46: (iv_ruleFactor= ruleFactor EOF )
            // InternalRax.g:379:2: iv_ruleFactor= ruleFactor EOF
            {
             newCompositeNode(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFactor=ruleFactor();

            state._fsp--;

             current =iv_ruleFactor.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFactor"


    // $ANTLR start "ruleFactor"
    // InternalRax.g:385:1: ruleFactor returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleFactor() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;


        	enterRule();

        try {
            // InternalRax.g:391:2: ( (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) )
            // InternalRax.g:392:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            {
            // InternalRax.g:392:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            // InternalRax.g:393:3: this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_17); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getFactorAccess().getINTTerminalRuleCall_0());
            		
            kw=(Token)match(input,19,FOLLOW_15); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getFactorAccess().getFullStopKeyword_1());
            		
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_2);
            		

            			newLeafNode(this_INT_2, grammarAccess.getFactorAccess().getINTTerminalRuleCall_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFactor"


    // $ANTLR start "entryRuleRole"
    // InternalRax.g:416:1: entryRuleRole returns [EObject current=null] : iv_ruleRole= ruleRole EOF ;
    public final EObject entryRuleRole() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRole = null;


        try {
            // InternalRax.g:416:45: (iv_ruleRole= ruleRole EOF )
            // InternalRax.g:417:2: iv_ruleRole= ruleRole EOF
            {
             newCompositeNode(grammarAccess.getRoleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRole=ruleRole();

            state._fsp--;

             current =iv_ruleRole; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalRax.g:423:1: ruleRole returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'for' ( (lv_factor_2_0= ruleFactor ) ) ) ;
    public final EObject ruleRole() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_factor_2_0 = null;



        	enterRule();

        try {
            // InternalRax.g:429:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'for' ( (lv_factor_2_0= ruleFactor ) ) ) )
            // InternalRax.g:430:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'for' ( (lv_factor_2_0= ruleFactor ) ) )
            {
            // InternalRax.g:430:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'for' ( (lv_factor_2_0= ruleFactor ) ) )
            // InternalRax.g:431:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= 'for' ( (lv_factor_2_0= ruleFactor ) )
            {
            // InternalRax.g:431:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalRax.g:432:4: (lv_name_0_0= RULE_ID )
            {
            // InternalRax.g:432:4: (lv_name_0_0= RULE_ID )
            // InternalRax.g:433:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_18); 

            					newLeafNode(lv_name_0_0, grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRoleRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,20,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getRoleAccess().getForKeyword_1());
            		
            // InternalRax.g:453:3: ( (lv_factor_2_0= ruleFactor ) )
            // InternalRax.g:454:4: (lv_factor_2_0= ruleFactor )
            {
            // InternalRax.g:454:4: (lv_factor_2_0= ruleFactor )
            // InternalRax.g:455:5: lv_factor_2_0= ruleFactor
            {

            					newCompositeNode(grammarAccess.getRoleAccess().getFactorFactorParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_factor_2_0=ruleFactor();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRoleRule());
            					}
            					set(
            						current,
            						"factor",
            						lv_factor_2_0,
            						"tdt4250.ra.Rax.Factor");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleID_OR_STRING"
    // InternalRax.g:476:1: entryRuleID_OR_STRING returns [String current=null] : iv_ruleID_OR_STRING= ruleID_OR_STRING EOF ;
    public final String entryRuleID_OR_STRING() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleID_OR_STRING = null;


        try {
            // InternalRax.g:476:52: (iv_ruleID_OR_STRING= ruleID_OR_STRING EOF )
            // InternalRax.g:477:2: iv_ruleID_OR_STRING= ruleID_OR_STRING EOF
            {
             newCompositeNode(grammarAccess.getID_OR_STRINGRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleID_OR_STRING=ruleID_OR_STRING();

            state._fsp--;

             current =iv_ruleID_OR_STRING.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleID_OR_STRING"


    // $ANTLR start "ruleID_OR_STRING"
    // InternalRax.g:483:1: ruleID_OR_STRING returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | this_STRING_1= RULE_STRING ) ;
    public final AntlrDatatypeRuleToken ruleID_OR_STRING() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token this_STRING_1=null;


        	enterRule();

        try {
            // InternalRax.g:489:2: ( (this_ID_0= RULE_ID | this_STRING_1= RULE_STRING ) )
            // InternalRax.g:490:2: (this_ID_0= RULE_ID | this_STRING_1= RULE_STRING )
            {
            // InternalRax.g:490:2: (this_ID_0= RULE_ID | this_STRING_1= RULE_STRING )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_STRING) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalRax.g:491:3: this_ID_0= RULE_ID
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_0);
                    		

                    			newLeafNode(this_ID_0, grammarAccess.getID_OR_STRINGAccess().getIDTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalRax.g:499:3: this_STRING_1= RULE_STRING
                    {
                    this_STRING_1=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_1);
                    		

                    			newLeafNode(this_STRING_1, grammarAccess.getID_OR_STRINGAccess().getSTRINGTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleID_OR_STRING"


    // $ANTLR start "entryRulePerson"
    // InternalRax.g:510:1: entryRulePerson returns [EObject current=null] : iv_rulePerson= rulePerson EOF ;
    public final EObject entryRulePerson() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePerson = null;


        try {
            // InternalRax.g:510:47: (iv_rulePerson= rulePerson EOF )
            // InternalRax.g:511:2: iv_rulePerson= rulePerson EOF
            {
             newCompositeNode(grammarAccess.getPersonRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePerson=rulePerson();

            state._fsp--;

             current =iv_rulePerson; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePerson"


    // $ANTLR start "rulePerson"
    // InternalRax.g:517:1: rulePerson returns [EObject current=null] : ( (lv_name_0_0= ruleID_OR_STRING ) ) ;
    public final EObject rulePerson() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalRax.g:523:2: ( ( (lv_name_0_0= ruleID_OR_STRING ) ) )
            // InternalRax.g:524:2: ( (lv_name_0_0= ruleID_OR_STRING ) )
            {
            // InternalRax.g:524:2: ( (lv_name_0_0= ruleID_OR_STRING ) )
            // InternalRax.g:525:3: (lv_name_0_0= ruleID_OR_STRING )
            {
            // InternalRax.g:525:3: (lv_name_0_0= ruleID_OR_STRING )
            // InternalRax.g:526:4: lv_name_0_0= ruleID_OR_STRING
            {

            				newCompositeNode(grammarAccess.getPersonAccess().getNameID_OR_STRINGParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleID_OR_STRING();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getPersonRule());
            				}
            				set(
            					current,
            					"name",
            					lv_name_0_0,
            					"tdt4250.ra.Rax.ID_OR_STRING");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePerson"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000820L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000100000L});

}