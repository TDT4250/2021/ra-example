/**
 */
package tdt4250.ra.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeUtil;
import tdt4250.ra.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see tdt4250.ra.RaPackage
 * @generated
 */
public class RaValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final RaValidator INSTANCE = new RaValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "tdt4250.ra";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return RaPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case RaPackage.COURSE:
				return validateCourse((Course)value, diagnostics, context);
			case RaPackage.PERSON:
				return validatePerson((Person)value, diagnostics, context);
			case RaPackage.RESOURCE_ALLOCATION:
				return validateResourceAllocation((ResourceAllocation)value, diagnostics, context);
			case RaPackage.DEPARTMENT:
				return validateDepartment((Department)value, diagnostics, context);
			case RaPackage.ROLE:
				return validateRole((Role)value, diagnostics, context);
			case RaPackage.COURSE_CODE:
				return validateCourseCode((String)value, diagnostics, context);
			case RaPackage.FACTOR:
				return validateFactor((Float)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(course, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_needsEnoughResources(course, diagnostics, context);
		if (result || diagnostics != null) result &= validateCourse_rolesMustBeFilled(course, diagnostics, context);
		return result;
	}

	/**
	 * Validates the needsEnoughResources constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateCourse_needsEnoughResources(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean violated = false;
		for (Role role : course.getRoles()) {
			float sum = 0.0f;
			for (ResourceAllocation allocation : course.getResourceAllocations()) {
				if (allocation.getRole() == role) {
					sum += allocation.getFactor();
				}
			}
			if (sum < 0.9f) {
				if (diagnostics != null) {
					diagnostics.add
						(createDiagnostic
							(Diagnostic.ERROR,
							 DIAGNOSTIC_SOURCE,
							 0,
							 "_UI_GenericConstraint_diagnostic",
							 new Object[] { "needsEnoughResources", getObjectLabel(course, context) },
							 new Object[] { course },
							 context));
					violated = true;
				} else {
					return false;
				}
			}
		}
		return ! violated;
	}

	/**
	 * The cached validation expression for the rolesMustBeFilled constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COURSE__ROLES_MUST_BE_FILLED__EEXPRESSION = "self.roles->collect(role | let factors = self.resourceAllocations->select(ra | ra.role = role).factor in (if factors->size() = 0 then 0.0 else factors->sum() endif))->forAll(sum | sum > 0.9)";

	/**
	 * Validates the rolesMustBeFilled constraint of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourse_rolesMustBeFilled(Course course, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RaPackage.Literals.COURSE,
				 course,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "rolesMustBeFilled",
				 COURSE__ROLES_MUST_BE_FILLED__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(person, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(person, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(person, diagnostics, context);
		if (result || diagnostics != null) result &= validatePerson_noWorkOverload(person, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the noWorkOverload constraint of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PERSON__NO_WORK_OVERLOAD__EEXPRESSION = "let ws = self.resourceAllocations->collect(ra | ra.factor * ra.role.factor) in (if ws->size() = 0 then 0.0 else ws->sum() endif) < Sequence{self.capacity}->sum() * 1.1";

	/**
	 * Validates the noWorkOverload constraint of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePerson_noWorkOverload(Person person, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RaPackage.Literals.PERSON,
				 person,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "noWorkOverload",
				 PERSON__NO_WORK_OVERLOAD__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocation(ResourceAllocation resourceAllocation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(resourceAllocation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(resourceAllocation, diagnostics, context);
		if (result || diagnostics != null) result &= validateResourceAllocation_resourceAllocationRoleMustBeForSameCourse(resourceAllocation, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the resourceAllocationRoleMustBeForSameCourse constraint of '<em>Resource Allocation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RESOURCE_ALLOCATION__RESOURCE_ALLOCATION_ROLE_MUST_BE_FOR_SAME_COURSE__EEXPRESSION = "self.role.eContainer() = self.course";

	/**
	 * Validates the resourceAllocationRoleMustBeForSameCourse constraint of '<em>Resource Allocation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceAllocation_resourceAllocationRoleMustBeForSameCourse(ResourceAllocation resourceAllocation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RaPackage.Literals.RESOURCE_ALLOCATION,
				 resourceAllocation,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "resourceAllocationRoleMustBeForSameCourse",
				 RESOURCE_ALLOCATION__RESOURCE_ALLOCATION_ROLE_MUST_BE_FOR_SAME_COURSE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDepartment(Department department, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(department, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(role, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseCode(String courseCode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateCourseCode_Pattern(courseCode, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateCourseCode_Pattern
	 */
	public static final  PatternMatcher [][] COURSE_CODE__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("\\p{L}+\\d+")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Course Code</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCourseCode_Pattern(String courseCode, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(RaPackage.Literals.COURSE_CODE, courseCode, COURSE_CODE__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor(float factor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateFactor_Min(factor, diagnostics, context);
		if (result || diagnostics != null) result &= validateFactor_Max(factor, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateFactor_Min
	 */
	public static final float FACTOR__MIN__VALUE = 0.0F;

	/**
	 * Validates the Min constraint of '<em>Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor_Min(float factor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = factor > FACTOR__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(RaPackage.Literals.FACTOR, factor, FACTOR__MIN__VALUE, false, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateFactor_Max
	 */
	public static final float FACTOR__MAX__VALUE = 2.0F;

	/**
	 * Validates the Max constraint of '<em>Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor_Max(float factor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = factor <= FACTOR__MAX__VALUE;
		if (!result && diagnostics != null)
			reportMaxViolation(RaPackage.Literals.FACTOR, factor, FACTOR__MAX__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //RaValidator
