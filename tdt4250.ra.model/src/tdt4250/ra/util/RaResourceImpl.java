/**
 */
package tdt4250.ra.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emfcloud.jackson.resource.JsonResource;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see tdt4250.ra.util.RaResourceFactoryImpl
 * @generated NOT
 */
public class RaResourceImpl extends JsonResource {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public RaResourceImpl(URI uri) {
		super(uri);
	}

} //RaResourceImpl
