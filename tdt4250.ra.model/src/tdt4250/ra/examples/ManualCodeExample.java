package tdt4250.ra.examples;

import tdt4250.ra.Person;
import tdt4250.ra.RaFactory;

public class ManualCodeExample {

	public static void main(String[] args) {
		Person person = RaFactory.eINSTANCE.createPerson();
		System.out.println(person.getName() + " == " + person.getGivenName() + " + " + person.getFamilyName());
		person.setName("Trætteberg");
		System.out.println(person.getName() + " == " + person.getGivenName() + " + " + person.getFamilyName());
		person.setName("Hallvard Trætteberg");
		System.out.println(person.getName() + " == " + person.getGivenName() + " + " + person.getFamilyName());
	}
}
