package tdt4250.ra.examples;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emfcloud.jackson.resource.JsonResourceFactory;

import tdt4250.ra.Course;
import tdt4250.ra.Department;
import tdt4250.ra.RaPackage;
import tdt4250.ra.util.RaResourceFactoryImpl;

public class JsonConversionExample {

	public static void main(String[] args) {
		ResourceSet resSet = new ResourceSetImpl();
		// relate the RaPackage identifier used in XMI files to the RaPackage instance (EPackage meta-object) 
		resSet.getPackageRegistry().put(RaPackage.eNS_URI, RaPackage.eINSTANCE);
		// relate the XMI parser to the 'xmi' file extension
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

		Resource resource = resSet.getResource(URI.createURI(JsonConversionExample.class.getResource("LoadResourceExample.xmi").toString()), true);
		Department dep = (Department) resource.getContents().get(0);
		for (Course course : dep.getCourses()) {
			System.out.println(course);
		}
		
		Resource converted = new JsonResourceFactory().createResource(resource.getURI().trimFileExtension().appendFileExtension("json"));
		converted.getContents().addAll(resource.getContents());
		try {
			converted.save(System.out, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
