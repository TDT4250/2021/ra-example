package tdt4250.ra.examples;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import tdt4250.ra.Department;
import tdt4250.ra.RaPackage;

public class ReflectiveResourceExample {

	public static void main(String[] args) {
		ResourceSet resSet = new ResourceSetImpl();
		// relate the RaPackage identifier used in XMI files to the RaPackage instance (EPackage meta-object) 
		resSet.getPackageRegistry().put(RaPackage.eNS_URI, RaPackage.eINSTANCE);
		// relate the XMI parser to the 'xmi' file extension
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

		Resource resource = resSet.getResource(URI.createURI(ReflectiveResourceExample.class.getResource("LoadResourceExample.xmi").toString()), true);
		Department dep = (Department) resource.getContents().get(0);
		serialize(dep, 0);
	}

	private static void indent(int level) {
		while (level-- > 0) {
			System.out.print("  ");
		}
	}
	
	/*
	IDI/courses/feature=TDT4250/roles/factor>0.5
	*/
	private static void serialize(EObject eObject, int level) {
		EClass eClass = eObject.eClass();
		indent(level);
		System.out.println("{");
		for (EAttribute attr : eClass.getEAllAttributes()) {
			if (shouldSuppress(attr)) {
			} else if (! attr.isTransient()) {
				indent(level + 1);
				System.out.print("\"" + attr.getName() + "\": ");
				if (attr.isMany()) {
					serializeValues((List<Object>) eObject.eGet(attr), level);
				} else {
					serializeValue(eObject.eGet(attr), level);				
				}
			}
		}
		for (EReference ref : eClass.getEAllReferences()) {
			if (shouldSuppress(ref)) {
			} else if (ref.isContainer()) {
				// up
			} else if (ref.isContainment()) {
				indent(level + 1);
				System.out.print("\"" + ref.getName() + "\": ");
				// down
				if (ref.isMany()) {
					serializeValues((List<Object>) eObject.eGet(ref), level);
				} else {
					serializeValue(eObject.eGet(ref), level);
				}
			} else {
				indent(level + 1);
				System.out.print("\"" + ref.getName() + "\": ");
				// across
				System.out.println("#");
			}
		}
		indent(level);
		System.out.println("}");
	}

	private static boolean shouldSuppress(EStructuralFeature feature) {
		EAnnotation eAnnotation = feature.getEAnnotation("jsonSerializer");
		if (eAnnotation != null && "true".equals(eAnnotation.getDetails().get("suppress"))) {
			return true;
		}
		return false;
	}

	private static void serializeValue(Object object, int level) {
		if (object instanceof EObject) {
			serialize((EObject) object, level + 1);
		} else {
			System.out.println("\"" + object + "\"");
		}
	}

	private static void serializeValues(List<Object> objects, int level) {
		System.out.print("[");
		for (Object object : objects) {
			serializeValue(object, level);
		}
		indent(level);
		System.out.println("]");
	}
}
