[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.idi.ntnu.no/#https://gitlab.stud.idi.ntnu.no/TDT4250/ra-example) 

# Resource allocation example

The resource allocation is meant to show most of the features of Ecore and EMF,
while keeping the model pretty simple.

## The resource allocation domain

The context for resource allocation in this example is university courses.

A *department* has a *staff* which is responsible for running a set of *courses*.
Each course has specific needs for staff playing different *roles*, e.g. responsible, lecturer, coordinator, TAs etc.
And each staff member has a contract stating how much time should be devoted to teaching.

The problem is creating a set of allocations that satisfies the course needs,
while not overburdening the staff. The focus of this example is that of representing the domain,
not automating the allocation process.

## Platform and architecture

We use Eclipse as the platform and provide an Ecore model and two (alternative) DSLs,
one diagram-based using Sirius and one text-based using Xtext. We don't bother (yet)
to define an RCP product, although that would be natural in a real-world setting.

## The ecore model

The core of the model is courses, staff and resource allocations, which can be thought of
as an association class linking the two, with additional data about allocation factor.

The ecore model is defined in the [tdt4250.ra.model](tdt4250.ra.model/README.md) project.

## The Sirius DSL

We use Sirius for visualising and editing the network of courses, staff and resource allocations.

The Sirius viewpoint specification can be found in the [tdt4250.ra.diagram](tdt4250.ra.diagram/README.md) project.

## The Xtext DSL

Alternatively, we can use a text-based representation defined by Xtext.

The Xtext grammar specification can be found in the [tdt4250.ra.xtext](tdt4250.ra.xtext/README.md) project.
