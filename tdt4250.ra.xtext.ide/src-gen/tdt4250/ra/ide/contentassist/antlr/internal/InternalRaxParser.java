package tdt4250.ra.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import tdt4250.ra.services.RaxGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRaxParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'-'", "'Persons:'", "'Courses:'", "'requires'", "'provided-by'", "','", "'is'", "'as'", "'.'", "'for'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__20=20;

    // delegates
    // delegators


        public InternalRaxParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRaxParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRaxParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRax.g"; }


    	private RaxGrammarAccess grammarAccess;

    	public void setGrammarAccess(RaxGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDepartment"
    // InternalRax.g:53:1: entryRuleDepartment : ruleDepartment EOF ;
    public final void entryRuleDepartment() throws RecognitionException {
        try {
            // InternalRax.g:54:1: ( ruleDepartment EOF )
            // InternalRax.g:55:1: ruleDepartment EOF
            {
             before(grammarAccess.getDepartmentRule()); 
            pushFollow(FOLLOW_1);
            ruleDepartment();

            state._fsp--;

             after(grammarAccess.getDepartmentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDepartment"


    // $ANTLR start "ruleDepartment"
    // InternalRax.g:62:1: ruleDepartment : ( ( rule__Department__Group__0 ) ) ;
    public final void ruleDepartment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:66:2: ( ( ( rule__Department__Group__0 ) ) )
            // InternalRax.g:67:2: ( ( rule__Department__Group__0 ) )
            {
            // InternalRax.g:67:2: ( ( rule__Department__Group__0 ) )
            // InternalRax.g:68:3: ( rule__Department__Group__0 )
            {
             before(grammarAccess.getDepartmentAccess().getGroup()); 
            // InternalRax.g:69:3: ( rule__Department__Group__0 )
            // InternalRax.g:69:4: rule__Department__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Department__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDepartmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDepartment"


    // $ANTLR start "entryRuleCourse"
    // InternalRax.g:78:1: entryRuleCourse : ruleCourse EOF ;
    public final void entryRuleCourse() throws RecognitionException {
        try {
            // InternalRax.g:79:1: ( ruleCourse EOF )
            // InternalRax.g:80:1: ruleCourse EOF
            {
             before(grammarAccess.getCourseRule()); 
            pushFollow(FOLLOW_1);
            ruleCourse();

            state._fsp--;

             after(grammarAccess.getCourseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCourse"


    // $ANTLR start "ruleCourse"
    // InternalRax.g:87:1: ruleCourse : ( ( rule__Course__Group__0 ) ) ;
    public final void ruleCourse() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:91:2: ( ( ( rule__Course__Group__0 ) ) )
            // InternalRax.g:92:2: ( ( rule__Course__Group__0 ) )
            {
            // InternalRax.g:92:2: ( ( rule__Course__Group__0 ) )
            // InternalRax.g:93:3: ( rule__Course__Group__0 )
            {
             before(grammarAccess.getCourseAccess().getGroup()); 
            // InternalRax.g:94:3: ( rule__Course__Group__0 )
            // InternalRax.g:94:4: rule__Course__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Course__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCourse"


    // $ANTLR start "entryRuleResourceAllocation"
    // InternalRax.g:103:1: entryRuleResourceAllocation : ruleResourceAllocation EOF ;
    public final void entryRuleResourceAllocation() throws RecognitionException {
        try {
            // InternalRax.g:104:1: ( ruleResourceAllocation EOF )
            // InternalRax.g:105:1: ruleResourceAllocation EOF
            {
             before(grammarAccess.getResourceAllocationRule()); 
            pushFollow(FOLLOW_1);
            ruleResourceAllocation();

            state._fsp--;

             after(grammarAccess.getResourceAllocationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleResourceAllocation"


    // $ANTLR start "ruleResourceAllocation"
    // InternalRax.g:112:1: ruleResourceAllocation : ( ( rule__ResourceAllocation__Group__0 ) ) ;
    public final void ruleResourceAllocation() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:116:2: ( ( ( rule__ResourceAllocation__Group__0 ) ) )
            // InternalRax.g:117:2: ( ( rule__ResourceAllocation__Group__0 ) )
            {
            // InternalRax.g:117:2: ( ( rule__ResourceAllocation__Group__0 ) )
            // InternalRax.g:118:3: ( rule__ResourceAllocation__Group__0 )
            {
             before(grammarAccess.getResourceAllocationAccess().getGroup()); 
            // InternalRax.g:119:3: ( rule__ResourceAllocation__Group__0 )
            // InternalRax.g:119:4: rule__ResourceAllocation__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getResourceAllocationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResourceAllocation"


    // $ANTLR start "entryRuleFactor"
    // InternalRax.g:128:1: entryRuleFactor : ruleFactor EOF ;
    public final void entryRuleFactor() throws RecognitionException {
        try {
            // InternalRax.g:129:1: ( ruleFactor EOF )
            // InternalRax.g:130:1: ruleFactor EOF
            {
             before(grammarAccess.getFactorRule()); 
            pushFollow(FOLLOW_1);
            ruleFactor();

            state._fsp--;

             after(grammarAccess.getFactorRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFactor"


    // $ANTLR start "ruleFactor"
    // InternalRax.g:137:1: ruleFactor : ( ( rule__Factor__Group__0 ) ) ;
    public final void ruleFactor() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:141:2: ( ( ( rule__Factor__Group__0 ) ) )
            // InternalRax.g:142:2: ( ( rule__Factor__Group__0 ) )
            {
            // InternalRax.g:142:2: ( ( rule__Factor__Group__0 ) )
            // InternalRax.g:143:3: ( rule__Factor__Group__0 )
            {
             before(grammarAccess.getFactorAccess().getGroup()); 
            // InternalRax.g:144:3: ( rule__Factor__Group__0 )
            // InternalRax.g:144:4: rule__Factor__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Factor__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFactorAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFactor"


    // $ANTLR start "entryRuleRole"
    // InternalRax.g:153:1: entryRuleRole : ruleRole EOF ;
    public final void entryRuleRole() throws RecognitionException {
        try {
            // InternalRax.g:154:1: ( ruleRole EOF )
            // InternalRax.g:155:1: ruleRole EOF
            {
             before(grammarAccess.getRoleRule()); 
            pushFollow(FOLLOW_1);
            ruleRole();

            state._fsp--;

             after(grammarAccess.getRoleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRole"


    // $ANTLR start "ruleRole"
    // InternalRax.g:162:1: ruleRole : ( ( rule__Role__Group__0 ) ) ;
    public final void ruleRole() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:166:2: ( ( ( rule__Role__Group__0 ) ) )
            // InternalRax.g:167:2: ( ( rule__Role__Group__0 ) )
            {
            // InternalRax.g:167:2: ( ( rule__Role__Group__0 ) )
            // InternalRax.g:168:3: ( rule__Role__Group__0 )
            {
             before(grammarAccess.getRoleAccess().getGroup()); 
            // InternalRax.g:169:3: ( rule__Role__Group__0 )
            // InternalRax.g:169:4: rule__Role__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Role__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRoleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRole"


    // $ANTLR start "entryRuleID_OR_STRING"
    // InternalRax.g:178:1: entryRuleID_OR_STRING : ruleID_OR_STRING EOF ;
    public final void entryRuleID_OR_STRING() throws RecognitionException {
        try {
            // InternalRax.g:179:1: ( ruleID_OR_STRING EOF )
            // InternalRax.g:180:1: ruleID_OR_STRING EOF
            {
             before(grammarAccess.getID_OR_STRINGRule()); 
            pushFollow(FOLLOW_1);
            ruleID_OR_STRING();

            state._fsp--;

             after(grammarAccess.getID_OR_STRINGRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleID_OR_STRING"


    // $ANTLR start "ruleID_OR_STRING"
    // InternalRax.g:187:1: ruleID_OR_STRING : ( ( rule__ID_OR_STRING__Alternatives ) ) ;
    public final void ruleID_OR_STRING() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:191:2: ( ( ( rule__ID_OR_STRING__Alternatives ) ) )
            // InternalRax.g:192:2: ( ( rule__ID_OR_STRING__Alternatives ) )
            {
            // InternalRax.g:192:2: ( ( rule__ID_OR_STRING__Alternatives ) )
            // InternalRax.g:193:3: ( rule__ID_OR_STRING__Alternatives )
            {
             before(grammarAccess.getID_OR_STRINGAccess().getAlternatives()); 
            // InternalRax.g:194:3: ( rule__ID_OR_STRING__Alternatives )
            // InternalRax.g:194:4: rule__ID_OR_STRING__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ID_OR_STRING__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getID_OR_STRINGAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleID_OR_STRING"


    // $ANTLR start "entryRulePerson"
    // InternalRax.g:203:1: entryRulePerson : rulePerson EOF ;
    public final void entryRulePerson() throws RecognitionException {
        try {
            // InternalRax.g:204:1: ( rulePerson EOF )
            // InternalRax.g:205:1: rulePerson EOF
            {
             before(grammarAccess.getPersonRule()); 
            pushFollow(FOLLOW_1);
            rulePerson();

            state._fsp--;

             after(grammarAccess.getPersonRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePerson"


    // $ANTLR start "rulePerson"
    // InternalRax.g:212:1: rulePerson : ( ( rule__Person__NameAssignment ) ) ;
    public final void rulePerson() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:216:2: ( ( ( rule__Person__NameAssignment ) ) )
            // InternalRax.g:217:2: ( ( rule__Person__NameAssignment ) )
            {
            // InternalRax.g:217:2: ( ( rule__Person__NameAssignment ) )
            // InternalRax.g:218:3: ( rule__Person__NameAssignment )
            {
             before(grammarAccess.getPersonAccess().getNameAssignment()); 
            // InternalRax.g:219:3: ( rule__Person__NameAssignment )
            // InternalRax.g:219:4: rule__Person__NameAssignment
            {
            pushFollow(FOLLOW_2);
            rule__Person__NameAssignment();

            state._fsp--;


            }

             after(grammarAccess.getPersonAccess().getNameAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePerson"


    // $ANTLR start "rule__ID_OR_STRING__Alternatives"
    // InternalRax.g:227:1: rule__ID_OR_STRING__Alternatives : ( ( RULE_ID ) | ( RULE_STRING ) );
    public final void rule__ID_OR_STRING__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:231:1: ( ( RULE_ID ) | ( RULE_STRING ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                alt1=1;
            }
            else if ( (LA1_0==RULE_STRING) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalRax.g:232:2: ( RULE_ID )
                    {
                    // InternalRax.g:232:2: ( RULE_ID )
                    // InternalRax.g:233:3: RULE_ID
                    {
                     before(grammarAccess.getID_OR_STRINGAccess().getIDTerminalRuleCall_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getID_OR_STRINGAccess().getIDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRax.g:238:2: ( RULE_STRING )
                    {
                    // InternalRax.g:238:2: ( RULE_STRING )
                    // InternalRax.g:239:3: RULE_STRING
                    {
                     before(grammarAccess.getID_OR_STRINGAccess().getSTRINGTerminalRuleCall_1()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getID_OR_STRINGAccess().getSTRINGTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ID_OR_STRING__Alternatives"


    // $ANTLR start "rule__Department__Group__0"
    // InternalRax.g:248:1: rule__Department__Group__0 : rule__Department__Group__0__Impl rule__Department__Group__1 ;
    public final void rule__Department__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:252:1: ( rule__Department__Group__0__Impl rule__Department__Group__1 )
            // InternalRax.g:253:2: rule__Department__Group__0__Impl rule__Department__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Department__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__0"


    // $ANTLR start "rule__Department__Group__0__Impl"
    // InternalRax.g:260:1: rule__Department__Group__0__Impl : ( ( rule__Department__ShortNameAssignment_0 ) ) ;
    public final void rule__Department__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:264:1: ( ( ( rule__Department__ShortNameAssignment_0 ) ) )
            // InternalRax.g:265:1: ( ( rule__Department__ShortNameAssignment_0 ) )
            {
            // InternalRax.g:265:1: ( ( rule__Department__ShortNameAssignment_0 ) )
            // InternalRax.g:266:2: ( rule__Department__ShortNameAssignment_0 )
            {
             before(grammarAccess.getDepartmentAccess().getShortNameAssignment_0()); 
            // InternalRax.g:267:2: ( rule__Department__ShortNameAssignment_0 )
            // InternalRax.g:267:3: rule__Department__ShortNameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Department__ShortNameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDepartmentAccess().getShortNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__0__Impl"


    // $ANTLR start "rule__Department__Group__1"
    // InternalRax.g:275:1: rule__Department__Group__1 : rule__Department__Group__1__Impl rule__Department__Group__2 ;
    public final void rule__Department__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:279:1: ( rule__Department__Group__1__Impl rule__Department__Group__2 )
            // InternalRax.g:280:2: rule__Department__Group__1__Impl rule__Department__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Department__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__1"


    // $ANTLR start "rule__Department__Group__1__Impl"
    // InternalRax.g:287:1: rule__Department__Group__1__Impl : ( '-' ) ;
    public final void rule__Department__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:291:1: ( ( '-' ) )
            // InternalRax.g:292:1: ( '-' )
            {
            // InternalRax.g:292:1: ( '-' )
            // InternalRax.g:293:2: '-'
            {
             before(grammarAccess.getDepartmentAccess().getHyphenMinusKeyword_1()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getDepartmentAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__1__Impl"


    // $ANTLR start "rule__Department__Group__2"
    // InternalRax.g:302:1: rule__Department__Group__2 : rule__Department__Group__2__Impl rule__Department__Group__3 ;
    public final void rule__Department__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:306:1: ( rule__Department__Group__2__Impl rule__Department__Group__3 )
            // InternalRax.g:307:2: rule__Department__Group__2__Impl rule__Department__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Department__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__2"


    // $ANTLR start "rule__Department__Group__2__Impl"
    // InternalRax.g:314:1: rule__Department__Group__2__Impl : ( ( rule__Department__NameAssignment_2 ) ) ;
    public final void rule__Department__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:318:1: ( ( ( rule__Department__NameAssignment_2 ) ) )
            // InternalRax.g:319:1: ( ( rule__Department__NameAssignment_2 ) )
            {
            // InternalRax.g:319:1: ( ( rule__Department__NameAssignment_2 ) )
            // InternalRax.g:320:2: ( rule__Department__NameAssignment_2 )
            {
             before(grammarAccess.getDepartmentAccess().getNameAssignment_2()); 
            // InternalRax.g:321:2: ( rule__Department__NameAssignment_2 )
            // InternalRax.g:321:3: rule__Department__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Department__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDepartmentAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__2__Impl"


    // $ANTLR start "rule__Department__Group__3"
    // InternalRax.g:329:1: rule__Department__Group__3 : rule__Department__Group__3__Impl rule__Department__Group__4 ;
    public final void rule__Department__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:333:1: ( rule__Department__Group__3__Impl rule__Department__Group__4 )
            // InternalRax.g:334:2: rule__Department__Group__3__Impl rule__Department__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Department__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__3"


    // $ANTLR start "rule__Department__Group__3__Impl"
    // InternalRax.g:341:1: rule__Department__Group__3__Impl : ( 'Persons:' ) ;
    public final void rule__Department__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:345:1: ( ( 'Persons:' ) )
            // InternalRax.g:346:1: ( 'Persons:' )
            {
            // InternalRax.g:346:1: ( 'Persons:' )
            // InternalRax.g:347:2: 'Persons:'
            {
             before(grammarAccess.getDepartmentAccess().getPersonsKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDepartmentAccess().getPersonsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__3__Impl"


    // $ANTLR start "rule__Department__Group__4"
    // InternalRax.g:356:1: rule__Department__Group__4 : rule__Department__Group__4__Impl rule__Department__Group__5 ;
    public final void rule__Department__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:360:1: ( rule__Department__Group__4__Impl rule__Department__Group__5 )
            // InternalRax.g:361:2: rule__Department__Group__4__Impl rule__Department__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Department__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__4"


    // $ANTLR start "rule__Department__Group__4__Impl"
    // InternalRax.g:368:1: rule__Department__Group__4__Impl : ( ( rule__Department__StaffAssignment_4 )* ) ;
    public final void rule__Department__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:372:1: ( ( ( rule__Department__StaffAssignment_4 )* ) )
            // InternalRax.g:373:1: ( ( rule__Department__StaffAssignment_4 )* )
            {
            // InternalRax.g:373:1: ( ( rule__Department__StaffAssignment_4 )* )
            // InternalRax.g:374:2: ( rule__Department__StaffAssignment_4 )*
            {
             before(grammarAccess.getDepartmentAccess().getStaffAssignment_4()); 
            // InternalRax.g:375:2: ( rule__Department__StaffAssignment_4 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=RULE_ID && LA2_0<=RULE_STRING)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRax.g:375:3: rule__Department__StaffAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Department__StaffAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getDepartmentAccess().getStaffAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__4__Impl"


    // $ANTLR start "rule__Department__Group__5"
    // InternalRax.g:383:1: rule__Department__Group__5 : rule__Department__Group__5__Impl rule__Department__Group__6 ;
    public final void rule__Department__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:387:1: ( rule__Department__Group__5__Impl rule__Department__Group__6 )
            // InternalRax.g:388:2: rule__Department__Group__5__Impl rule__Department__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Department__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Department__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__5"


    // $ANTLR start "rule__Department__Group__5__Impl"
    // InternalRax.g:395:1: rule__Department__Group__5__Impl : ( 'Courses:' ) ;
    public final void rule__Department__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:399:1: ( ( 'Courses:' ) )
            // InternalRax.g:400:1: ( 'Courses:' )
            {
            // InternalRax.g:400:1: ( 'Courses:' )
            // InternalRax.g:401:2: 'Courses:'
            {
             before(grammarAccess.getDepartmentAccess().getCoursesKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDepartmentAccess().getCoursesKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__5__Impl"


    // $ANTLR start "rule__Department__Group__6"
    // InternalRax.g:410:1: rule__Department__Group__6 : rule__Department__Group__6__Impl ;
    public final void rule__Department__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:414:1: ( rule__Department__Group__6__Impl )
            // InternalRax.g:415:2: rule__Department__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Department__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__6"


    // $ANTLR start "rule__Department__Group__6__Impl"
    // InternalRax.g:421:1: rule__Department__Group__6__Impl : ( ( rule__Department__CoursesAssignment_6 )* ) ;
    public final void rule__Department__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:425:1: ( ( ( rule__Department__CoursesAssignment_6 )* ) )
            // InternalRax.g:426:1: ( ( rule__Department__CoursesAssignment_6 )* )
            {
            // InternalRax.g:426:1: ( ( rule__Department__CoursesAssignment_6 )* )
            // InternalRax.g:427:2: ( rule__Department__CoursesAssignment_6 )*
            {
             before(grammarAccess.getDepartmentAccess().getCoursesAssignment_6()); 
            // InternalRax.g:428:2: ( rule__Department__CoursesAssignment_6 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalRax.g:428:3: rule__Department__CoursesAssignment_6
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Department__CoursesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getDepartmentAccess().getCoursesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__Group__6__Impl"


    // $ANTLR start "rule__Course__Group__0"
    // InternalRax.g:437:1: rule__Course__Group__0 : rule__Course__Group__0__Impl rule__Course__Group__1 ;
    public final void rule__Course__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:441:1: ( rule__Course__Group__0__Impl rule__Course__Group__1 )
            // InternalRax.g:442:2: rule__Course__Group__0__Impl rule__Course__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Course__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__0"


    // $ANTLR start "rule__Course__Group__0__Impl"
    // InternalRax.g:449:1: rule__Course__Group__0__Impl : ( ( rule__Course__CodeAssignment_0 ) ) ;
    public final void rule__Course__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:453:1: ( ( ( rule__Course__CodeAssignment_0 ) ) )
            // InternalRax.g:454:1: ( ( rule__Course__CodeAssignment_0 ) )
            {
            // InternalRax.g:454:1: ( ( rule__Course__CodeAssignment_0 ) )
            // InternalRax.g:455:2: ( rule__Course__CodeAssignment_0 )
            {
             before(grammarAccess.getCourseAccess().getCodeAssignment_0()); 
            // InternalRax.g:456:2: ( rule__Course__CodeAssignment_0 )
            // InternalRax.g:456:3: rule__Course__CodeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Course__CodeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getCodeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__0__Impl"


    // $ANTLR start "rule__Course__Group__1"
    // InternalRax.g:464:1: rule__Course__Group__1 : rule__Course__Group__1__Impl rule__Course__Group__2 ;
    public final void rule__Course__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:468:1: ( rule__Course__Group__1__Impl rule__Course__Group__2 )
            // InternalRax.g:469:2: rule__Course__Group__1__Impl rule__Course__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Course__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__1"


    // $ANTLR start "rule__Course__Group__1__Impl"
    // InternalRax.g:476:1: rule__Course__Group__1__Impl : ( ( '-' )? ) ;
    public final void rule__Course__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:480:1: ( ( ( '-' )? ) )
            // InternalRax.g:481:1: ( ( '-' )? )
            {
            // InternalRax.g:481:1: ( ( '-' )? )
            // InternalRax.g:482:2: ( '-' )?
            {
             before(grammarAccess.getCourseAccess().getHyphenMinusKeyword_1()); 
            // InternalRax.g:483:2: ( '-' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==11) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRax.g:483:3: '-'
                    {
                    match(input,11,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getCourseAccess().getHyphenMinusKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__1__Impl"


    // $ANTLR start "rule__Course__Group__2"
    // InternalRax.g:491:1: rule__Course__Group__2 : rule__Course__Group__2__Impl rule__Course__Group__3 ;
    public final void rule__Course__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:495:1: ( rule__Course__Group__2__Impl rule__Course__Group__3 )
            // InternalRax.g:496:2: rule__Course__Group__2__Impl rule__Course__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Course__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__2"


    // $ANTLR start "rule__Course__Group__2__Impl"
    // InternalRax.g:503:1: rule__Course__Group__2__Impl : ( ( rule__Course__NameAssignment_2 ) ) ;
    public final void rule__Course__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:507:1: ( ( ( rule__Course__NameAssignment_2 ) ) )
            // InternalRax.g:508:1: ( ( rule__Course__NameAssignment_2 ) )
            {
            // InternalRax.g:508:1: ( ( rule__Course__NameAssignment_2 ) )
            // InternalRax.g:509:2: ( rule__Course__NameAssignment_2 )
            {
             before(grammarAccess.getCourseAccess().getNameAssignment_2()); 
            // InternalRax.g:510:2: ( rule__Course__NameAssignment_2 )
            // InternalRax.g:510:3: rule__Course__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Course__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__2__Impl"


    // $ANTLR start "rule__Course__Group__3"
    // InternalRax.g:518:1: rule__Course__Group__3 : rule__Course__Group__3__Impl ;
    public final void rule__Course__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:522:1: ( rule__Course__Group__3__Impl )
            // InternalRax.g:523:2: rule__Course__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Course__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__3"


    // $ANTLR start "rule__Course__Group__3__Impl"
    // InternalRax.g:529:1: rule__Course__Group__3__Impl : ( ( rule__Course__Group_3__0 )? ) ;
    public final void rule__Course__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:533:1: ( ( ( rule__Course__Group_3__0 )? ) )
            // InternalRax.g:534:1: ( ( rule__Course__Group_3__0 )? )
            {
            // InternalRax.g:534:1: ( ( rule__Course__Group_3__0 )? )
            // InternalRax.g:535:2: ( rule__Course__Group_3__0 )?
            {
             before(grammarAccess.getCourseAccess().getGroup_3()); 
            // InternalRax.g:536:2: ( rule__Course__Group_3__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==14) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalRax.g:536:3: rule__Course__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Course__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCourseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group__3__Impl"


    // $ANTLR start "rule__Course__Group_3__0"
    // InternalRax.g:545:1: rule__Course__Group_3__0 : rule__Course__Group_3__0__Impl rule__Course__Group_3__1 ;
    public final void rule__Course__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:549:1: ( rule__Course__Group_3__0__Impl rule__Course__Group_3__1 )
            // InternalRax.g:550:2: rule__Course__Group_3__0__Impl rule__Course__Group_3__1
            {
            pushFollow(FOLLOW_8);
            rule__Course__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__0"


    // $ANTLR start "rule__Course__Group_3__0__Impl"
    // InternalRax.g:557:1: rule__Course__Group_3__0__Impl : ( 'requires' ) ;
    public final void rule__Course__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:561:1: ( ( 'requires' ) )
            // InternalRax.g:562:1: ( 'requires' )
            {
            // InternalRax.g:562:1: ( 'requires' )
            // InternalRax.g:563:2: 'requires'
            {
             before(grammarAccess.getCourseAccess().getRequiresKeyword_3_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getCourseAccess().getRequiresKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__0__Impl"


    // $ANTLR start "rule__Course__Group_3__1"
    // InternalRax.g:572:1: rule__Course__Group_3__1 : rule__Course__Group_3__1__Impl rule__Course__Group_3__2 ;
    public final void rule__Course__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:576:1: ( rule__Course__Group_3__1__Impl rule__Course__Group_3__2 )
            // InternalRax.g:577:2: rule__Course__Group_3__1__Impl rule__Course__Group_3__2
            {
            pushFollow(FOLLOW_12);
            rule__Course__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__1"


    // $ANTLR start "rule__Course__Group_3__1__Impl"
    // InternalRax.g:584:1: rule__Course__Group_3__1__Impl : ( ( rule__Course__RolesAssignment_3_1 ) ) ;
    public final void rule__Course__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:588:1: ( ( ( rule__Course__RolesAssignment_3_1 ) ) )
            // InternalRax.g:589:1: ( ( rule__Course__RolesAssignment_3_1 ) )
            {
            // InternalRax.g:589:1: ( ( rule__Course__RolesAssignment_3_1 ) )
            // InternalRax.g:590:2: ( rule__Course__RolesAssignment_3_1 )
            {
             before(grammarAccess.getCourseAccess().getRolesAssignment_3_1()); 
            // InternalRax.g:591:2: ( rule__Course__RolesAssignment_3_1 )
            // InternalRax.g:591:3: rule__Course__RolesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Course__RolesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getRolesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__1__Impl"


    // $ANTLR start "rule__Course__Group_3__2"
    // InternalRax.g:599:1: rule__Course__Group_3__2 : rule__Course__Group_3__2__Impl rule__Course__Group_3__3 ;
    public final void rule__Course__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:603:1: ( rule__Course__Group_3__2__Impl rule__Course__Group_3__3 )
            // InternalRax.g:604:2: rule__Course__Group_3__2__Impl rule__Course__Group_3__3
            {
            pushFollow(FOLLOW_12);
            rule__Course__Group_3__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group_3__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__2"


    // $ANTLR start "rule__Course__Group_3__2__Impl"
    // InternalRax.g:611:1: rule__Course__Group_3__2__Impl : ( ( rule__Course__Group_3_2__0 )* ) ;
    public final void rule__Course__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:615:1: ( ( ( rule__Course__Group_3_2__0 )* ) )
            // InternalRax.g:616:1: ( ( rule__Course__Group_3_2__0 )* )
            {
            // InternalRax.g:616:1: ( ( rule__Course__Group_3_2__0 )* )
            // InternalRax.g:617:2: ( rule__Course__Group_3_2__0 )*
            {
             before(grammarAccess.getCourseAccess().getGroup_3_2()); 
            // InternalRax.g:618:2: ( rule__Course__Group_3_2__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalRax.g:618:3: rule__Course__Group_3_2__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__Course__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getCourseAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__2__Impl"


    // $ANTLR start "rule__Course__Group_3__3"
    // InternalRax.g:626:1: rule__Course__Group_3__3 : rule__Course__Group_3__3__Impl rule__Course__Group_3__4 ;
    public final void rule__Course__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:630:1: ( rule__Course__Group_3__3__Impl rule__Course__Group_3__4 )
            // InternalRax.g:631:2: rule__Course__Group_3__3__Impl rule__Course__Group_3__4
            {
            pushFollow(FOLLOW_14);
            rule__Course__Group_3__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group_3__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__3"


    // $ANTLR start "rule__Course__Group_3__3__Impl"
    // InternalRax.g:638:1: rule__Course__Group_3__3__Impl : ( 'provided-by' ) ;
    public final void rule__Course__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:642:1: ( ( 'provided-by' ) )
            // InternalRax.g:643:1: ( 'provided-by' )
            {
            // InternalRax.g:643:1: ( 'provided-by' )
            // InternalRax.g:644:2: 'provided-by'
            {
             before(grammarAccess.getCourseAccess().getProvidedByKeyword_3_3()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getCourseAccess().getProvidedByKeyword_3_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__3__Impl"


    // $ANTLR start "rule__Course__Group_3__4"
    // InternalRax.g:653:1: rule__Course__Group_3__4 : rule__Course__Group_3__4__Impl ;
    public final void rule__Course__Group_3__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:657:1: ( rule__Course__Group_3__4__Impl )
            // InternalRax.g:658:2: rule__Course__Group_3__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Course__Group_3__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__4"


    // $ANTLR start "rule__Course__Group_3__4__Impl"
    // InternalRax.g:664:1: rule__Course__Group_3__4__Impl : ( ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) ) ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* ) ) ;
    public final void rule__Course__Group_3__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:668:1: ( ( ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) ) ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* ) ) )
            // InternalRax.g:669:1: ( ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) ) ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* ) )
            {
            // InternalRax.g:669:1: ( ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) ) ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* ) )
            // InternalRax.g:670:2: ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) ) ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* )
            {
            // InternalRax.g:670:2: ( ( rule__Course__ResourceAllocationsAssignment_3_4 ) )
            // InternalRax.g:671:3: ( rule__Course__ResourceAllocationsAssignment_3_4 )
            {
             before(grammarAccess.getCourseAccess().getResourceAllocationsAssignment_3_4()); 
            // InternalRax.g:672:3: ( rule__Course__ResourceAllocationsAssignment_3_4 )
            // InternalRax.g:672:4: rule__Course__ResourceAllocationsAssignment_3_4
            {
            pushFollow(FOLLOW_7);
            rule__Course__ResourceAllocationsAssignment_3_4();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getResourceAllocationsAssignment_3_4()); 

            }

            // InternalRax.g:675:2: ( ( rule__Course__ResourceAllocationsAssignment_3_4 )* )
            // InternalRax.g:676:3: ( rule__Course__ResourceAllocationsAssignment_3_4 )*
            {
             before(grammarAccess.getCourseAccess().getResourceAllocationsAssignment_3_4()); 
            // InternalRax.g:677:3: ( rule__Course__ResourceAllocationsAssignment_3_4 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID) ) {
                    int LA7_2 = input.LA(2);

                    if ( (LA7_2==17) ) {
                        alt7=1;
                    }


                }
                else if ( (LA7_0==RULE_STRING) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalRax.g:677:4: rule__Course__ResourceAllocationsAssignment_3_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Course__ResourceAllocationsAssignment_3_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getCourseAccess().getResourceAllocationsAssignment_3_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3__4__Impl"


    // $ANTLR start "rule__Course__Group_3_2__0"
    // InternalRax.g:687:1: rule__Course__Group_3_2__0 : rule__Course__Group_3_2__0__Impl rule__Course__Group_3_2__1 ;
    public final void rule__Course__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:691:1: ( rule__Course__Group_3_2__0__Impl rule__Course__Group_3_2__1 )
            // InternalRax.g:692:2: rule__Course__Group_3_2__0__Impl rule__Course__Group_3_2__1
            {
            pushFollow(FOLLOW_8);
            rule__Course__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Course__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3_2__0"


    // $ANTLR start "rule__Course__Group_3_2__0__Impl"
    // InternalRax.g:699:1: rule__Course__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__Course__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:703:1: ( ( ',' ) )
            // InternalRax.g:704:1: ( ',' )
            {
            // InternalRax.g:704:1: ( ',' )
            // InternalRax.g:705:2: ','
            {
             before(grammarAccess.getCourseAccess().getCommaKeyword_3_2_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCourseAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3_2__0__Impl"


    // $ANTLR start "rule__Course__Group_3_2__1"
    // InternalRax.g:714:1: rule__Course__Group_3_2__1 : rule__Course__Group_3_2__1__Impl ;
    public final void rule__Course__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:718:1: ( rule__Course__Group_3_2__1__Impl )
            // InternalRax.g:719:2: rule__Course__Group_3_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Course__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3_2__1"


    // $ANTLR start "rule__Course__Group_3_2__1__Impl"
    // InternalRax.g:725:1: rule__Course__Group_3_2__1__Impl : ( ( rule__Course__RolesAssignment_3_2_1 ) ) ;
    public final void rule__Course__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:729:1: ( ( ( rule__Course__RolesAssignment_3_2_1 ) ) )
            // InternalRax.g:730:1: ( ( rule__Course__RolesAssignment_3_2_1 ) )
            {
            // InternalRax.g:730:1: ( ( rule__Course__RolesAssignment_3_2_1 ) )
            // InternalRax.g:731:2: ( rule__Course__RolesAssignment_3_2_1 )
            {
             before(grammarAccess.getCourseAccess().getRolesAssignment_3_2_1()); 
            // InternalRax.g:732:2: ( rule__Course__RolesAssignment_3_2_1 )
            // InternalRax.g:732:3: rule__Course__RolesAssignment_3_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Course__RolesAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getCourseAccess().getRolesAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__Group_3_2__1__Impl"


    // $ANTLR start "rule__ResourceAllocation__Group__0"
    // InternalRax.g:741:1: rule__ResourceAllocation__Group__0 : rule__ResourceAllocation__Group__0__Impl rule__ResourceAllocation__Group__1 ;
    public final void rule__ResourceAllocation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:745:1: ( rule__ResourceAllocation__Group__0__Impl rule__ResourceAllocation__Group__1 )
            // InternalRax.g:746:2: rule__ResourceAllocation__Group__0__Impl rule__ResourceAllocation__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__ResourceAllocation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__0"


    // $ANTLR start "rule__ResourceAllocation__Group__0__Impl"
    // InternalRax.g:753:1: rule__ResourceAllocation__Group__0__Impl : ( ( rule__ResourceAllocation__PersonAssignment_0 ) ) ;
    public final void rule__ResourceAllocation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:757:1: ( ( ( rule__ResourceAllocation__PersonAssignment_0 ) ) )
            // InternalRax.g:758:1: ( ( rule__ResourceAllocation__PersonAssignment_0 ) )
            {
            // InternalRax.g:758:1: ( ( rule__ResourceAllocation__PersonAssignment_0 ) )
            // InternalRax.g:759:2: ( rule__ResourceAllocation__PersonAssignment_0 )
            {
             before(grammarAccess.getResourceAllocationAccess().getPersonAssignment_0()); 
            // InternalRax.g:760:2: ( rule__ResourceAllocation__PersonAssignment_0 )
            // InternalRax.g:760:3: rule__ResourceAllocation__PersonAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__PersonAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getResourceAllocationAccess().getPersonAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__0__Impl"


    // $ANTLR start "rule__ResourceAllocation__Group__1"
    // InternalRax.g:768:1: rule__ResourceAllocation__Group__1 : rule__ResourceAllocation__Group__1__Impl rule__ResourceAllocation__Group__2 ;
    public final void rule__ResourceAllocation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:772:1: ( rule__ResourceAllocation__Group__1__Impl rule__ResourceAllocation__Group__2 )
            // InternalRax.g:773:2: rule__ResourceAllocation__Group__1__Impl rule__ResourceAllocation__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__ResourceAllocation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__1"


    // $ANTLR start "rule__ResourceAllocation__Group__1__Impl"
    // InternalRax.g:780:1: rule__ResourceAllocation__Group__1__Impl : ( 'is' ) ;
    public final void rule__ResourceAllocation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:784:1: ( ( 'is' ) )
            // InternalRax.g:785:1: ( 'is' )
            {
            // InternalRax.g:785:1: ( 'is' )
            // InternalRax.g:786:2: 'is'
            {
             before(grammarAccess.getResourceAllocationAccess().getIsKeyword_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getResourceAllocationAccess().getIsKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__1__Impl"


    // $ANTLR start "rule__ResourceAllocation__Group__2"
    // InternalRax.g:795:1: rule__ResourceAllocation__Group__2 : rule__ResourceAllocation__Group__2__Impl rule__ResourceAllocation__Group__3 ;
    public final void rule__ResourceAllocation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:799:1: ( rule__ResourceAllocation__Group__2__Impl rule__ResourceAllocation__Group__3 )
            // InternalRax.g:800:2: rule__ResourceAllocation__Group__2__Impl rule__ResourceAllocation__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__ResourceAllocation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__2"


    // $ANTLR start "rule__ResourceAllocation__Group__2__Impl"
    // InternalRax.g:807:1: rule__ResourceAllocation__Group__2__Impl : ( ( rule__ResourceAllocation__FactorAssignment_2 ) ) ;
    public final void rule__ResourceAllocation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:811:1: ( ( ( rule__ResourceAllocation__FactorAssignment_2 ) ) )
            // InternalRax.g:812:1: ( ( rule__ResourceAllocation__FactorAssignment_2 ) )
            {
            // InternalRax.g:812:1: ( ( rule__ResourceAllocation__FactorAssignment_2 ) )
            // InternalRax.g:813:2: ( rule__ResourceAllocation__FactorAssignment_2 )
            {
             before(grammarAccess.getResourceAllocationAccess().getFactorAssignment_2()); 
            // InternalRax.g:814:2: ( rule__ResourceAllocation__FactorAssignment_2 )
            // InternalRax.g:814:3: rule__ResourceAllocation__FactorAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__FactorAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getResourceAllocationAccess().getFactorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__2__Impl"


    // $ANTLR start "rule__ResourceAllocation__Group__3"
    // InternalRax.g:822:1: rule__ResourceAllocation__Group__3 : rule__ResourceAllocation__Group__3__Impl rule__ResourceAllocation__Group__4 ;
    public final void rule__ResourceAllocation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:826:1: ( rule__ResourceAllocation__Group__3__Impl rule__ResourceAllocation__Group__4 )
            // InternalRax.g:827:2: rule__ResourceAllocation__Group__3__Impl rule__ResourceAllocation__Group__4
            {
            pushFollow(FOLLOW_8);
            rule__ResourceAllocation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__3"


    // $ANTLR start "rule__ResourceAllocation__Group__3__Impl"
    // InternalRax.g:834:1: rule__ResourceAllocation__Group__3__Impl : ( 'as' ) ;
    public final void rule__ResourceAllocation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:838:1: ( ( 'as' ) )
            // InternalRax.g:839:1: ( 'as' )
            {
            // InternalRax.g:839:1: ( 'as' )
            // InternalRax.g:840:2: 'as'
            {
             before(grammarAccess.getResourceAllocationAccess().getAsKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getResourceAllocationAccess().getAsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__3__Impl"


    // $ANTLR start "rule__ResourceAllocation__Group__4"
    // InternalRax.g:849:1: rule__ResourceAllocation__Group__4 : rule__ResourceAllocation__Group__4__Impl ;
    public final void rule__ResourceAllocation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:853:1: ( rule__ResourceAllocation__Group__4__Impl )
            // InternalRax.g:854:2: rule__ResourceAllocation__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__4"


    // $ANTLR start "rule__ResourceAllocation__Group__4__Impl"
    // InternalRax.g:860:1: rule__ResourceAllocation__Group__4__Impl : ( ( rule__ResourceAllocation__RoleAssignment_4 ) ) ;
    public final void rule__ResourceAllocation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:864:1: ( ( ( rule__ResourceAllocation__RoleAssignment_4 ) ) )
            // InternalRax.g:865:1: ( ( rule__ResourceAllocation__RoleAssignment_4 ) )
            {
            // InternalRax.g:865:1: ( ( rule__ResourceAllocation__RoleAssignment_4 ) )
            // InternalRax.g:866:2: ( rule__ResourceAllocation__RoleAssignment_4 )
            {
             before(grammarAccess.getResourceAllocationAccess().getRoleAssignment_4()); 
            // InternalRax.g:867:2: ( rule__ResourceAllocation__RoleAssignment_4 )
            // InternalRax.g:867:3: rule__ResourceAllocation__RoleAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__ResourceAllocation__RoleAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getResourceAllocationAccess().getRoleAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__Group__4__Impl"


    // $ANTLR start "rule__Factor__Group__0"
    // InternalRax.g:876:1: rule__Factor__Group__0 : rule__Factor__Group__0__Impl rule__Factor__Group__1 ;
    public final void rule__Factor__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:880:1: ( rule__Factor__Group__0__Impl rule__Factor__Group__1 )
            // InternalRax.g:881:2: rule__Factor__Group__0__Impl rule__Factor__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Factor__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Factor__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__0"


    // $ANTLR start "rule__Factor__Group__0__Impl"
    // InternalRax.g:888:1: rule__Factor__Group__0__Impl : ( RULE_INT ) ;
    public final void rule__Factor__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:892:1: ( ( RULE_INT ) )
            // InternalRax.g:893:1: ( RULE_INT )
            {
            // InternalRax.g:893:1: ( RULE_INT )
            // InternalRax.g:894:2: RULE_INT
            {
             before(grammarAccess.getFactorAccess().getINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getFactorAccess().getINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__0__Impl"


    // $ANTLR start "rule__Factor__Group__1"
    // InternalRax.g:903:1: rule__Factor__Group__1 : rule__Factor__Group__1__Impl rule__Factor__Group__2 ;
    public final void rule__Factor__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:907:1: ( rule__Factor__Group__1__Impl rule__Factor__Group__2 )
            // InternalRax.g:908:2: rule__Factor__Group__1__Impl rule__Factor__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Factor__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Factor__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__1"


    // $ANTLR start "rule__Factor__Group__1__Impl"
    // InternalRax.g:915:1: rule__Factor__Group__1__Impl : ( '.' ) ;
    public final void rule__Factor__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:919:1: ( ( '.' ) )
            // InternalRax.g:920:1: ( '.' )
            {
            // InternalRax.g:920:1: ( '.' )
            // InternalRax.g:921:2: '.'
            {
             before(grammarAccess.getFactorAccess().getFullStopKeyword_1()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getFactorAccess().getFullStopKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__1__Impl"


    // $ANTLR start "rule__Factor__Group__2"
    // InternalRax.g:930:1: rule__Factor__Group__2 : rule__Factor__Group__2__Impl ;
    public final void rule__Factor__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:934:1: ( rule__Factor__Group__2__Impl )
            // InternalRax.g:935:2: rule__Factor__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Factor__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__2"


    // $ANTLR start "rule__Factor__Group__2__Impl"
    // InternalRax.g:941:1: rule__Factor__Group__2__Impl : ( RULE_INT ) ;
    public final void rule__Factor__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:945:1: ( ( RULE_INT ) )
            // InternalRax.g:946:1: ( RULE_INT )
            {
            // InternalRax.g:946:1: ( RULE_INT )
            // InternalRax.g:947:2: RULE_INT
            {
             before(grammarAccess.getFactorAccess().getINTTerminalRuleCall_2()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getFactorAccess().getINTTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Factor__Group__2__Impl"


    // $ANTLR start "rule__Role__Group__0"
    // InternalRax.g:957:1: rule__Role__Group__0 : rule__Role__Group__0__Impl rule__Role__Group__1 ;
    public final void rule__Role__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:961:1: ( rule__Role__Group__0__Impl rule__Role__Group__1 )
            // InternalRax.g:962:2: rule__Role__Group__0__Impl rule__Role__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Role__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Role__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__0"


    // $ANTLR start "rule__Role__Group__0__Impl"
    // InternalRax.g:969:1: rule__Role__Group__0__Impl : ( ( rule__Role__NameAssignment_0 ) ) ;
    public final void rule__Role__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:973:1: ( ( ( rule__Role__NameAssignment_0 ) ) )
            // InternalRax.g:974:1: ( ( rule__Role__NameAssignment_0 ) )
            {
            // InternalRax.g:974:1: ( ( rule__Role__NameAssignment_0 ) )
            // InternalRax.g:975:2: ( rule__Role__NameAssignment_0 )
            {
             before(grammarAccess.getRoleAccess().getNameAssignment_0()); 
            // InternalRax.g:976:2: ( rule__Role__NameAssignment_0 )
            // InternalRax.g:976:3: rule__Role__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Role__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getRoleAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__0__Impl"


    // $ANTLR start "rule__Role__Group__1"
    // InternalRax.g:984:1: rule__Role__Group__1 : rule__Role__Group__1__Impl rule__Role__Group__2 ;
    public final void rule__Role__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:988:1: ( rule__Role__Group__1__Impl rule__Role__Group__2 )
            // InternalRax.g:989:2: rule__Role__Group__1__Impl rule__Role__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Role__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Role__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__1"


    // $ANTLR start "rule__Role__Group__1__Impl"
    // InternalRax.g:996:1: rule__Role__Group__1__Impl : ( 'for' ) ;
    public final void rule__Role__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1000:1: ( ( 'for' ) )
            // InternalRax.g:1001:1: ( 'for' )
            {
            // InternalRax.g:1001:1: ( 'for' )
            // InternalRax.g:1002:2: 'for'
            {
             before(grammarAccess.getRoleAccess().getForKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getRoleAccess().getForKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__1__Impl"


    // $ANTLR start "rule__Role__Group__2"
    // InternalRax.g:1011:1: rule__Role__Group__2 : rule__Role__Group__2__Impl ;
    public final void rule__Role__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1015:1: ( rule__Role__Group__2__Impl )
            // InternalRax.g:1016:2: rule__Role__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Role__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__2"


    // $ANTLR start "rule__Role__Group__2__Impl"
    // InternalRax.g:1022:1: rule__Role__Group__2__Impl : ( ( rule__Role__FactorAssignment_2 ) ) ;
    public final void rule__Role__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1026:1: ( ( ( rule__Role__FactorAssignment_2 ) ) )
            // InternalRax.g:1027:1: ( ( rule__Role__FactorAssignment_2 ) )
            {
            // InternalRax.g:1027:1: ( ( rule__Role__FactorAssignment_2 ) )
            // InternalRax.g:1028:2: ( rule__Role__FactorAssignment_2 )
            {
             before(grammarAccess.getRoleAccess().getFactorAssignment_2()); 
            // InternalRax.g:1029:2: ( rule__Role__FactorAssignment_2 )
            // InternalRax.g:1029:3: rule__Role__FactorAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Role__FactorAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRoleAccess().getFactorAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__Group__2__Impl"


    // $ANTLR start "rule__Department__ShortNameAssignment_0"
    // InternalRax.g:1038:1: rule__Department__ShortNameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Department__ShortNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1042:1: ( ( RULE_ID ) )
            // InternalRax.g:1043:2: ( RULE_ID )
            {
            // InternalRax.g:1043:2: ( RULE_ID )
            // InternalRax.g:1044:3: RULE_ID
            {
             before(grammarAccess.getDepartmentAccess().getShortNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDepartmentAccess().getShortNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__ShortNameAssignment_0"


    // $ANTLR start "rule__Department__NameAssignment_2"
    // InternalRax.g:1053:1: rule__Department__NameAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Department__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1057:1: ( ( RULE_STRING ) )
            // InternalRax.g:1058:2: ( RULE_STRING )
            {
            // InternalRax.g:1058:2: ( RULE_STRING )
            // InternalRax.g:1059:3: RULE_STRING
            {
             before(grammarAccess.getDepartmentAccess().getNameSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getDepartmentAccess().getNameSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__NameAssignment_2"


    // $ANTLR start "rule__Department__StaffAssignment_4"
    // InternalRax.g:1068:1: rule__Department__StaffAssignment_4 : ( rulePerson ) ;
    public final void rule__Department__StaffAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1072:1: ( ( rulePerson ) )
            // InternalRax.g:1073:2: ( rulePerson )
            {
            // InternalRax.g:1073:2: ( rulePerson )
            // InternalRax.g:1074:3: rulePerson
            {
             before(grammarAccess.getDepartmentAccess().getStaffPersonParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            rulePerson();

            state._fsp--;

             after(grammarAccess.getDepartmentAccess().getStaffPersonParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__StaffAssignment_4"


    // $ANTLR start "rule__Department__CoursesAssignment_6"
    // InternalRax.g:1083:1: rule__Department__CoursesAssignment_6 : ( ruleCourse ) ;
    public final void rule__Department__CoursesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1087:1: ( ( ruleCourse ) )
            // InternalRax.g:1088:2: ( ruleCourse )
            {
            // InternalRax.g:1088:2: ( ruleCourse )
            // InternalRax.g:1089:3: ruleCourse
            {
             before(grammarAccess.getDepartmentAccess().getCoursesCourseParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCourse();

            state._fsp--;

             after(grammarAccess.getDepartmentAccess().getCoursesCourseParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Department__CoursesAssignment_6"


    // $ANTLR start "rule__Course__CodeAssignment_0"
    // InternalRax.g:1098:1: rule__Course__CodeAssignment_0 : ( RULE_ID ) ;
    public final void rule__Course__CodeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1102:1: ( ( RULE_ID ) )
            // InternalRax.g:1103:2: ( RULE_ID )
            {
            // InternalRax.g:1103:2: ( RULE_ID )
            // InternalRax.g:1104:3: RULE_ID
            {
             before(grammarAccess.getCourseAccess().getCodeIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCourseAccess().getCodeIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__CodeAssignment_0"


    // $ANTLR start "rule__Course__NameAssignment_2"
    // InternalRax.g:1113:1: rule__Course__NameAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Course__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1117:1: ( ( RULE_STRING ) )
            // InternalRax.g:1118:2: ( RULE_STRING )
            {
            // InternalRax.g:1118:2: ( RULE_STRING )
            // InternalRax.g:1119:3: RULE_STRING
            {
             before(grammarAccess.getCourseAccess().getNameSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getCourseAccess().getNameSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__NameAssignment_2"


    // $ANTLR start "rule__Course__RolesAssignment_3_1"
    // InternalRax.g:1128:1: rule__Course__RolesAssignment_3_1 : ( ruleRole ) ;
    public final void rule__Course__RolesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1132:1: ( ( ruleRole ) )
            // InternalRax.g:1133:2: ( ruleRole )
            {
            // InternalRax.g:1133:2: ( ruleRole )
            // InternalRax.g:1134:3: ruleRole
            {
             before(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRole();

            state._fsp--;

             after(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__RolesAssignment_3_1"


    // $ANTLR start "rule__Course__RolesAssignment_3_2_1"
    // InternalRax.g:1143:1: rule__Course__RolesAssignment_3_2_1 : ( ruleRole ) ;
    public final void rule__Course__RolesAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1147:1: ( ( ruleRole ) )
            // InternalRax.g:1148:2: ( ruleRole )
            {
            // InternalRax.g:1148:2: ( ruleRole )
            // InternalRax.g:1149:3: ruleRole
            {
             before(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleRole();

            state._fsp--;

             after(grammarAccess.getCourseAccess().getRolesRoleParserRuleCall_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__RolesAssignment_3_2_1"


    // $ANTLR start "rule__Course__ResourceAllocationsAssignment_3_4"
    // InternalRax.g:1158:1: rule__Course__ResourceAllocationsAssignment_3_4 : ( ruleResourceAllocation ) ;
    public final void rule__Course__ResourceAllocationsAssignment_3_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1162:1: ( ( ruleResourceAllocation ) )
            // InternalRax.g:1163:2: ( ruleResourceAllocation )
            {
            // InternalRax.g:1163:2: ( ruleResourceAllocation )
            // InternalRax.g:1164:3: ruleResourceAllocation
            {
             before(grammarAccess.getCourseAccess().getResourceAllocationsResourceAllocationParserRuleCall_3_4_0()); 
            pushFollow(FOLLOW_2);
            ruleResourceAllocation();

            state._fsp--;

             after(grammarAccess.getCourseAccess().getResourceAllocationsResourceAllocationParserRuleCall_3_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Course__ResourceAllocationsAssignment_3_4"


    // $ANTLR start "rule__ResourceAllocation__PersonAssignment_0"
    // InternalRax.g:1173:1: rule__ResourceAllocation__PersonAssignment_0 : ( ( ruleID_OR_STRING ) ) ;
    public final void rule__ResourceAllocation__PersonAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1177:1: ( ( ( ruleID_OR_STRING ) ) )
            // InternalRax.g:1178:2: ( ( ruleID_OR_STRING ) )
            {
            // InternalRax.g:1178:2: ( ( ruleID_OR_STRING ) )
            // InternalRax.g:1179:3: ( ruleID_OR_STRING )
            {
             before(grammarAccess.getResourceAllocationAccess().getPersonPersonCrossReference_0_0()); 
            // InternalRax.g:1180:3: ( ruleID_OR_STRING )
            // InternalRax.g:1181:4: ruleID_OR_STRING
            {
             before(grammarAccess.getResourceAllocationAccess().getPersonPersonID_OR_STRINGParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleID_OR_STRING();

            state._fsp--;

             after(grammarAccess.getResourceAllocationAccess().getPersonPersonID_OR_STRINGParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getResourceAllocationAccess().getPersonPersonCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__PersonAssignment_0"


    // $ANTLR start "rule__ResourceAllocation__FactorAssignment_2"
    // InternalRax.g:1192:1: rule__ResourceAllocation__FactorAssignment_2 : ( ruleFactor ) ;
    public final void rule__ResourceAllocation__FactorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1196:1: ( ( ruleFactor ) )
            // InternalRax.g:1197:2: ( ruleFactor )
            {
            // InternalRax.g:1197:2: ( ruleFactor )
            // InternalRax.g:1198:3: ruleFactor
            {
             before(grammarAccess.getResourceAllocationAccess().getFactorFactorParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFactor();

            state._fsp--;

             after(grammarAccess.getResourceAllocationAccess().getFactorFactorParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__FactorAssignment_2"


    // $ANTLR start "rule__ResourceAllocation__RoleAssignment_4"
    // InternalRax.g:1207:1: rule__ResourceAllocation__RoleAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__ResourceAllocation__RoleAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1211:1: ( ( ( RULE_ID ) ) )
            // InternalRax.g:1212:2: ( ( RULE_ID ) )
            {
            // InternalRax.g:1212:2: ( ( RULE_ID ) )
            // InternalRax.g:1213:3: ( RULE_ID )
            {
             before(grammarAccess.getResourceAllocationAccess().getRoleRoleCrossReference_4_0()); 
            // InternalRax.g:1214:3: ( RULE_ID )
            // InternalRax.g:1215:4: RULE_ID
            {
             before(grammarAccess.getResourceAllocationAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getResourceAllocationAccess().getRoleRoleIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getResourceAllocationAccess().getRoleRoleCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceAllocation__RoleAssignment_4"


    // $ANTLR start "rule__Role__NameAssignment_0"
    // InternalRax.g:1226:1: rule__Role__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Role__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1230:1: ( ( RULE_ID ) )
            // InternalRax.g:1231:2: ( RULE_ID )
            {
            // InternalRax.g:1231:2: ( RULE_ID )
            // InternalRax.g:1232:3: RULE_ID
            {
             before(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRoleAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__NameAssignment_0"


    // $ANTLR start "rule__Role__FactorAssignment_2"
    // InternalRax.g:1241:1: rule__Role__FactorAssignment_2 : ( ruleFactor ) ;
    public final void rule__Role__FactorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1245:1: ( ( ruleFactor ) )
            // InternalRax.g:1246:2: ( ruleFactor )
            {
            // InternalRax.g:1246:2: ( ruleFactor )
            // InternalRax.g:1247:3: ruleFactor
            {
             before(grammarAccess.getRoleAccess().getFactorFactorParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFactor();

            state._fsp--;

             after(grammarAccess.getRoleAccess().getFactorFactorParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Role__FactorAssignment_2"


    // $ANTLR start "rule__Person__NameAssignment"
    // InternalRax.g:1256:1: rule__Person__NameAssignment : ( ruleID_OR_STRING ) ;
    public final void rule__Person__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRax.g:1260:1: ( ( ruleID_OR_STRING ) )
            // InternalRax.g:1261:2: ( ruleID_OR_STRING )
            {
            // InternalRax.g:1261:2: ( ruleID_OR_STRING )
            // InternalRax.g:1262:3: ruleID_OR_STRING
            {
             before(grammarAccess.getPersonAccess().getNameID_OR_STRINGParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleID_OR_STRING();

            state._fsp--;

             after(grammarAccess.getPersonAccess().getNameID_OR_STRINGParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Person__NameAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000820L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000100000L});

}