package tdt4250.ra.tests;

import java.util.List;

import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import junit.framework.TestCase;
import tdt4250.ra.Course;
import tdt4250.ra.Department;
import tdt4250.ra.Person;
import tdt4250.ra.RaPackage;
import tdt4250.ra.ResourceAllocation;

public class RaValidatorTest extends TestCase {

	private Resource testInstance;
	private Diagnostic diagnostics;
	
	protected Resource loadResource(String name) {
		ResourceSet resSet = new ResourceSetImpl();
		// relate the RaPackage identifier used in XMI files to the RaPackage instance (EPackage meta-object) 
		resSet.getPackageRegistry().put(RaPackage.eNS_URI, RaPackage.eINSTANCE);
		// relate the XMI parser to the 'xmi' file extension
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		
		return resSet.getResource(URI.createURI(RaValidatorTest.class.getResource(name + ".xmi").toString()), true);
	}
	
	protected void setUp() throws Exception {
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());

		testInstance = loadResource("RaValidatorTest");
		diagnostics = Diagnostician.INSTANCE.validate(testInstance.getContents().get(0));
	}
	
	private Diagnostic findDiagnostics(Diagnostic diagnostic, Object o, String constraint) {
		if (diagnostic.getMessage().contains(constraint) && (o == null || diagnostic.getData().contains(o))) {
			return diagnostic;
		}
		for (Diagnostic child : diagnostic.getChildren()) {
			Diagnostic found = findDiagnostics(child, o, constraint);
			if (found != null) {
				return found;
			}
		}
		return null;
	}
	
	public void testConstraint_rolesMustBeFilled() {
		Department dep = (Department) testInstance.getContents().get(0);
		Course tdt4250 = dep.getCourses().stream().filter(course -> course.getCode().equals("TDT4250")).findAny().get();
		Course it1901 = dep.getCourses().stream().filter(course -> course.getCode().equals("IT1901")).findAny().get();
		// violated for tdt4250
		assertNotNull(findDiagnostics(diagnostics, tdt4250, "rolesMustBeFilled"));
		// ok for it1901
		assertNull(findDiagnostics(diagnostics, it1901, "rolesMustBeFilled"));
	}

	public void testConstraint_noWorkOverload() {
		Department dep = (Department) testInstance.getContents().get(0);
		Person hallvard = dep.getStaff().stream().filter(course -> course.getName().equals("Hallvard Trætteberg")).findAny().get();
		Person adrian = dep.getStaff().stream().filter(course -> course.getName().equals("Adrian Stoica")).findAny().get();
		// violated for hallvard
		assertNotNull(findDiagnostics(diagnostics, hallvard, "noWorkOverload"));
		// ok for adrian
		assertNull(findDiagnostics(diagnostics, adrian, "noWorkOverload"));
	}

	// value is constrained to be in range <0.0, 2]
	public void testCourseCode_patternConstraints() {
		assertEquals(Diagnostic.ERROR, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getCourseCode(), "TDT").getSeverity());
		assertEquals(Diagnostic.ERROR, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getCourseCode(), "4250").getSeverity());
		assertEquals(Diagnostic.OK, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getCourseCode(), "TDT4250").getSeverity());
	}
	
	// value is constrained to be in range <0.0, 2]
	public void testFactor_valueRangeConstraints() {
		assertEquals(Diagnostic.ERROR, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getFactor(), 0.0f).getSeverity());
		assertEquals(Diagnostic.ERROR, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getFactor(), 2.1f).getSeverity());
		assertEquals(Diagnostic.OK, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getFactor(), 0.1f).getSeverity());
		assertEquals(Diagnostic.OK, Diagnostician.INSTANCE.validate(RaPackage.eINSTANCE.getFactor(), 2.0f).getSeverity());
	}
	
	public void testConstraint_resourceAllocationRoleMustBeForSameCourse() {
		Department dep = (Department) loadResource("testConstraint_resourceAllocationRoleMustBeForSameCourse").getContents().get(0);
		Course it1901 = dep.getCourses().stream().filter(course -> course.getCode().equals("IT1901")).findAny().get();
		// limit validation to specific object
		Diagnostic diagnostics = Diagnostician.INSTANCE.validate(it1901);
		List<ResourceAllocation> ras = it1901.getResourceAllocations();
		assertEquals(2, ras.size());
		// first ResourceAllocation is ok
		assertNull(findDiagnostics(diagnostics, ras.get(0), "resourceAllocationRoleMustBeForSameCourse"));
		// second ResourceAllocation violates constraint
		assertNotNull(findDiagnostics(diagnostics, ras.get(1), "resourceAllocationRoleMustBeForSameCourse"));
	}
}
