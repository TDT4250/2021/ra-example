/**
 */
package tdt4250.ra.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import tdt4250.ra.Course;
import tdt4250.ra.Department;
import tdt4250.ra.Person;
import tdt4250.ra.RaFactory;
import tdt4250.ra.ResourceAllocation;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link tdt4250.ra.Department#allocateResource(tdt4250.ra.Course, tdt4250.ra.Person, float) <em>Allocate Resource</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class DepartmentTest extends TestCase {

	/**
	 * The fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DepartmentTest.class);
	}

	/**
	 * Constructs a new Department test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DepartmentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Department fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Department test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Department getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(RaFactory.eINSTANCE.createDepartment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.ra.Department#allocateResource(tdt4250.ra.Course, tdt4250.ra.Person, float) <em>Allocate Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.ra.Department#allocateResource(tdt4250.ra.Course, tdt4250.ra.Person, float)
	 * @generated NOT
	 */
	public void testAllocateResource__Course_Person_float() {
		Course course = RaFactory.eINSTANCE.createCourse();
		Person person = RaFactory.eINSTANCE.createPerson();
		Department dep = RaFactory.eINSTANCE.createDepartment();
		dep.getCourses().add(course);
		dep.getStaff().add(person);
		dep.allocateResource(course, person, 0.5f);
		assertFalse(course.getResourceAllocations().isEmpty());
		assertEquals(1, course.getResourceAllocations().size());
		ResourceAllocation ra = course.getResourceAllocations().get(0);
		assertEquals(0.5f, ra.getFactor());
		assertEquals(1, person.getResourceAllocations().size());
		assertEquals(course, ra.getCourse());
		assertEquals(person, ra.getPerson());
	}

} //DepartmentTest
