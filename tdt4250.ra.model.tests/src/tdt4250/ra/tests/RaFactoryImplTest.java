package tdt4250.ra.tests;

import junit.framework.TestCase;
import tdt4250.ra.RaFactory;
import tdt4250.ra.RaPackage;

public class RaFactoryImplTest extends TestCase {

	// value is constrained to be in range <0.0, 2]
	public void testCourseCode_patternConstraints() {
		try {
			RaFactory.eINSTANCE.createFromString(RaPackage.eINSTANCE.getCourseCode(), "TDT");
			fail();
		} catch (IllegalArgumentException iae) {
		} catch (Exception e) {
			fail();
		}
		try {
			RaFactory.eINSTANCE.createFromString(RaPackage.eINSTANCE.getCourseCode(), "4250");
			fail();
		} catch (IllegalArgumentException iae) {
		} catch (Exception e) {
			fail();
		}
		try {
			RaFactory.eINSTANCE.createFromString(RaPackage.eINSTANCE.getCourseCode(), "TDT4250");
		} catch (Exception e) {
			fail();
		}
	}
}
