/**
 */
package tdt4250.ra.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import tdt4250.ra.Person;
import tdt4250.ra.RaFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link tdt4250.ra.Person#getGivenName() <em>Given Name</em>}</li>
 *   <li>{@link tdt4250.ra.Person#getFamilyName() <em>Family Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class PersonTest extends TestCase {

	/**
	 * The fixture for this Person test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Person fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PersonTest.class);
	}

	/**
	 * Constructs a new Person test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PersonTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Person test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Person fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Person test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Person getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(RaFactory.eINSTANCE.createPerson());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link tdt4250.ra.Person#getGivenName() <em>Given Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.ra.Person#getGivenName()
	 * @generated NOT
	 */
	public void testGetGivenName() {
		Person person = RaFactory.eINSTANCE.createPerson();
		person.setName("Hallvard Trætteberg");
		assertEquals("Hallvard", person.getGivenName());
		person.setName("Trætteberg");
		assertEquals("?", person.getGivenName());
	}

	/**
	 * Tests the '{@link tdt4250.ra.Person#setGivenName(java.lang.String) <em>Given Name</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.ra.Person#setGivenName(java.lang.String)
	 * @generated NOT
	 */
	public void testSetGivenName() {
		Person person = RaFactory.eINSTANCE.createPerson();
		person.setGivenName("Hallvard");
		assertEquals("Hallvard ?", person.getName());
		person.setName("Hallvard Trætteberg");
		person.setGivenName("Ola");
		assertEquals("Ola Trætteberg", person.getName());
	}

	/**
	 * Tests the '{@link tdt4250.ra.Person#getFamilyName() <em>Family Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.ra.Person#getFamilyName()
	 * @generated NOT
	 */
	public void testGetFamilyName() {
		Person person = RaFactory.eINSTANCE.createPerson();
		person.setName("Hallvard Trætteberg");
		assertEquals("Trætteberg", person.getFamilyName());
		person.setName("Trætteberg");
		assertEquals("Trætteberg", person.getFamilyName());
		person.setName(null);
		assertEquals("?", person.getFamilyName());
	}

	/**
	 * Tests the '{@link tdt4250.ra.Person#setFamilyName(java.lang.String) <em>Family Name</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.ra.Person#setFamilyName(java.lang.String)
	 * @generated
	 */
	public void testSetFamilyName() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //PersonTest
